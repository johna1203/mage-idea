package com.kodokux.mageidea;

import com.intellij.openapi.components.*;
import com.intellij.openapi.project.Project;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.Nullable;

@State(
        name = "MageIdeaSettings",
        storages = {
//                @Storage(id = "default", file = StoragePathMacros.APP_CONFIG),
//                @Storage(id = "dir", file = StoragePathMacros.APP_CONFIG + "/mage-idea.xml", scheme = StorageScheme.DIRECTORY_BASED)
                @Storage(id = "other", file = StoragePathMacros.APP_CONFIG + "/mage-idea.xml")
        }
)

public class Settings implements PersistentStateComponent<Settings> {

    public String user = "";
    public String name = "";
    public String email = "";
    public String modmanPath = "";

    public static Settings getInstance() {
        return ServiceManager.getService(Settings.class);
    }

    @Nullable
    @Override
    public Settings getState() {
        return this;
    }

    @Override
    public void loadState(Settings settings) {
        XmlSerializerUtil.copyBean(settings, this);
    }
}
