package com.kodokux.mageidea;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.util.io.FileUtilRt;
import com.intellij.openapi.vfs.*;
import com.kodokux.mageidea.modman.ModmanFile;
import com.kodokux.mageidea.modman.ModmanUtils;
import io.netty.util.internal.StringUtil;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/13
 * Time: 12:01
 */
public class MagentoComponent implements ProjectComponent {
    private final Project myProject;

    public MagentoComponent(Project project) {
        myProject = project;
    }

    public static MagentoComponent getInstance(Project project) {
        return project.getComponent(MagentoComponent.class);
    }


    @Override
    public void projectOpened() {
        MyVfsListener vfsListener = new MyVfsListener();
        VirtualFileManager.getInstance().addVirtualFileListener(vfsListener, myProject);
    }

    @Override
    public void projectClosed() {
    }

    @Override
    public void initComponent() {
        String configPath = myProject.getBasePath() + File.separator + ".idea" + File.separator + "mage-idea";
        File file = new File(configPath);
        if (!file.exists()) {
            if (file.mkdirs()) {
                LocalFileSystem.getInstance().refreshAndFindFileByIoFile(file);
            }
        }
    }

    @Override
    public void disposeComponent() {

    }

    @NotNull
    @Override
    public String getComponentName() {
        return "Com.kodokux.MagentoComponent";
    }

    private class MyVfsListener extends VirtualFileAdapter {
        public void fileCreated(VirtualFileEvent event) {
            //isModmanFile
            VirtualFile file = event.getFile();
            if (file.getName().equals(".git")) {
                VirtualFile extensionFile = file.getParent();
                if (extensionFile != null) {
                    VirtualFile modmanFile = extensionFile.getParent();
                    if (modmanFile != null && modmanFile.getName().equals(".modman")) {
                        ModmanUtils.refreshAndConfigureVcsMappings(myProject, extensionFile, extensionFile.getPath());
                    }
                }
            } else {
                if (file.getParent().getName().equals(".modman")) {
                    if (file.findChild(".git") != null) {
                        ModmanUtils.refreshAndConfigureVcsMappings(myProject, file, file.getPath());
                    }
                }
            }

            //updateModmanFile
            if (file.getPath().contains(".modman")) {
                String s = myProject.getBasePath() + "/.modman";
                String relativePath = FileUtil.getRelativePath(s, file.getPath(), '/');
                final String[] split = StringUtil.split(relativePath, '/');
                if (!file.isDirectory()) {
                    if (split.length > 0) {
                        doSymbolicLinks(split[0]);
                    }
                } else {
//                    Collection<VirtualFile> psiFiles = FilenameIndex.get
                    VirtualFileVisitor.Result fileInProvidedPath = VfsUtilCore.visitChildrenRecursively(file, new VirtualFileVisitor() {
                        @Override
                        public void afterChildrenVisited(@NotNull VirtualFile file) {
                            if (file.getName().equals("config.xml") && file.getParent().getName().equals("etc")) {
                                if (split.length > 0) {
                                    doSymbolicLinks(split[0]);
                                }
                            }
                        }
                    });
                }
            }
        }


        @Override
        public void beforeFileDeletion(VirtualFileEvent event) {
            VirtualFile file = event.getFile();
            if (file.getPath().contains(".modman")) {

                String s = myProject.getBasePath() + "/.modman";
                String relativePath = FileUtil.getRelativePath(s, file.getPath(), '/');

                File fileNative = null;
                if (file.getParent().getName().equals(".modman")) {
                    fileNative = new File(file.getPath());
                } else {
                    String[] split = StringUtil.split(relativePath, '/');
                    if (split.length > 0) {
                        fileNative = new File(myProject.getBasePath(), ".modman/" + split[0]);
                    }
                }

                if (fileNative != null) {
                    ModmanUtils.deleteSymbolicLinks(myProject, file, relativePath);

                    File modmanNativeFile = new File(fileNative, "modman");
                    ModmanFile modmanFile = new ModmanFile(myProject, fileNative, modmanNativeFile);
                    List<String> links = modmanFile.getLinks();
                    //ModmanUtils.updateModmanFile(myProject, fileNative, links);

                    //ModmanUtils.createSymbolicLinks(myProject, fileNative, links);
                }
            }

        }

        public void fileDeleted(VirtualFileEvent event) {
            VirtualFile file = event.getFile();
            if (file.isDirectory() && file.getParent() != null && file.getParent().getName().equals(".modman")) {
                if (file.findChild(".git") != null) {
                    ModmanUtils.refreshAndRemoveVcsMappings(myProject, file, file.getPath());
                }
            }

            if (file.getPath().contains(".modman")) {
                String s = myProject.getBasePath() + "/.modman";
                String relativePath = FileUtilRt.getRelativePath(s, file.getPath(), '/');
                String[] split = StringUtil.split(relativePath, '/');
                if (split.length > 0) {
                    doSymbolicLinks(split[0]);
                }
            }

        }
    }

    private void doSymbolicLinks(String s) {
        File fileNative = new File(myProject.getBasePath(), ".modman/" + s);
        File modmanNativeFile = new File(fileNative, "modman");
        ModmanFile modmanFile = new ModmanFile(myProject, fileNative, modmanNativeFile);
        List<String> links = modmanFile.getLinks();
        //ModmanUtils.updateModmanFile(myProject, fileNative, links);
        ModmanUtils.createSymbolicLinks(myProject, fileNative, links);
    }

    public void showInfoNotification(String content) {
        Notification errorNotification = new Notification("Mage idea", "Mage idea", content, NotificationType.INFORMATION);
        Notifications.Bus.notify(errorNotification, this.myProject);
    }

    static public void showInfoNotification(Project project, String content) {
        Notification errorNotification = new Notification("Mage idea", "Mage idea", content, NotificationType.INFORMATION);
        Notifications.Bus.notify(errorNotification, project);
    }

    static public void showInLogEvent(String title, String message, NotificationType type) {
        Notifications.Bus.notify(new Notification("Mageman", title, message, type));
    }

    static public void showInLogEvent(String title, String message) {
        Notifications.Bus.notify(new Notification("Mageman", title, message, NotificationType.INFORMATION));
    }

}
