package com.kodokux.mageidea.codeInsight;

import com.intellij.codeHighlighting.Pass;
import com.intellij.codeInsight.daemon.LineMarkerInfo;
import com.intellij.codeInsight.daemon.LineMarkerProvider;
import com.intellij.openapi.editor.markup.GutterIconRenderer;
import com.intellij.psi.PsiElement;
import com.kodokux.mageidea.Icons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/17
 * Time: 22:10
 */
public class MagentoLineMarkerProvider implements LineMarkerProvider {
    @Nullable
    @Override
    public LineMarkerInfo getLineMarkerInfo(@NotNull PsiElement psiElement) {
//        System.out.println("johna");

        return new LineMarkerInfo<PsiElement>(psiElement, psiElement.getTextRange(), Icons.MAGENTO_ICON_16, Pass.UPDATE_ALL, null, null,
                GutterIconRenderer.Alignment.LEFT);
    }

    @Override
    public void collectSlowLineMarkers(@NotNull List<PsiElement> psiElements, @NotNull Collection<LineMarkerInfo> lineMarkerInfos) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
