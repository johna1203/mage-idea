package com.kodokux.mageidea;

import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/11
 * Time: 19:50
 */
public class SettingsForm implements Configurable {
    //    private final Project project;
    private JPanel myPanel;
    private JTextField userTextField;
    private JTextField nameTextField;
    private JTextField emailTextField;
    private JTextField modmanPathTextField;

    @Nls
    @Override
    public String getDisplayName() {
        return "Mage Idea";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        return (JComponent) myPanel;
    }

    @Override
    public boolean isModified() {
        return !userTextField.getText().equals(getSettings().user)
                || !nameTextField.getText().equals(getSettings().name)
                || !emailTextField.getText().equals(getSettings().email)
                || !modmanPathTextField.getText().equals(getSettings().modmanPath)
                ;
    }

    @Override
    public void apply() throws ConfigurationException {
        getSettings().user = userTextField.getText();
        getSettings().name = nameTextField.getText();
        getSettings().email = emailTextField.getText();
        getSettings().modmanPath = modmanPathTextField.getText();
    }

    @Override
    public void reset() {
        updateUIFromSettings();
    }

    private void updateUIFromSettings() {
        userTextField.setText(getSettings().user);
        nameTextField.setText(getSettings().name);
        emailTextField.setText(getSettings().email);
        modmanPathTextField.setText(getSettings().modmanPath);
    }

    @Override
    public void disposeUIResources() {
    }

    private Settings getSettings() {
        return Settings.getInstance();
    }
}
