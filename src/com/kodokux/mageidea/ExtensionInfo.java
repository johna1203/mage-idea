package com.kodokux.mageidea;

import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.popup.JBPopup;
import com.intellij.ui.components.JBList;
import com.intellij.ui.popup.util.DetailViewImpl;
import com.intellij.ui.popup.util.ItemWrapper;
import com.intellij.ui.popup.util.MasterDetailPopupBuilder;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.event.KeyEvent;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/13
 * Time: 15:59
 */
public class ExtensionInfo extends AnAction implements DumbAware, MasterDetailPopupBuilder.Delegate {
    private JBPopup myPopup;

    public void actionPerformed(AnActionEvent e) {

        DataContext dataContext = e.getDataContext();
        final Project project = PlatformDataKeys.PROJECT.getData(dataContext);
        if (project == null) return;

        if (myPopup != null && myPopup.isVisible()) return;
        JBList list = new JBList();
        list.add(new JLabel("johna"));
        list.getEmptyText().setText("No Bookmarks");
        DefaultActionGroup actions = new DefaultActionGroup();
        myPopup = new MasterDetailPopupBuilder(project)
                .setActionsGroup(actions)
                .setList(list)
                .setDetailView(new DetailViewImpl(project))
                .setCloseOnEnter(false)
                .setDoneRunnable(new Runnable() {
                    @Override
                    public void run() {
                        myPopup.cancel();
                    }
                })
                .setDelegate(this).createMasterDetailPopup();
        myPopup.showCenteredInCurrentWindow(project);


//        DataContext context = e.getDataContext();
//        VirtualFile[] files = PlatformDataKeys.VIRTUAL_FILE_ARRAY.getData(context);
//
//        if (files == null || files.length != 1) {
//            return;
//        }

//        ConfigUtil configUtil = ConfigUtil.getInstance(project);
//
//        List<String> extensionList = configUtil.getExtensionList();
//
//        ExtensionInfoDialog dialog = new ExtensionInfoDialog(project, extensionList);
//        dialog.show();

//        Connection.main(null);


//        VirtualFile dir = files[0];
//        ProjectFileIndex fileIndex = ProjectRootManager.getInstance(e.getProject()).getFileIndex();

//
//        FindModel model = (FindModel) FindManager.getInstance(project).getFindInProjectModel();
//        model.setDirectoryName(project.getBasePath());
//        model.setGlobal(false);
//        model.setReplaceState(false);
//        model.setProjectScope(false);
//        model.setModuleName(null);
//        model.setWithSubdirectories(true);
//        model.setMultipleFiles(true);
//        model.setStringToFind("test test");
////        model.setFileFilter(fileFilter);

//        FindInProjectManager.getInstance(project).findInProject(context);

//        VirtualFile configXml = dir.findFileByRelativePath("etc/config.xml");
//
//        if (configXml != null) {
//            MageConfig config = new MageConfig(configXml.getPath());
//            ExtensionInfoDialog dialog = new ExtensionInfoDialog(project, config);
//            dialog.show();
//        }
//            try {
//                File fXmlFile = new File(configXml.getPath());
//                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//                Document doc = dBuilder.parse(fXmlFile);
//                NodeList modules = doc.getElementsByTagName("modules");
//
//                if (modules.getLength() > 1) {
//                    NodeList childNodes = modules.item(0).getChildNodes();
//                    for (int i = 0; i < childNodes.getLength(); i++) {
//                        if (childNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
//                            String moduleName = childNodes.item(i).getNodeName();
//                            System.out.println(moduleName);
//                            break;
//                        }
//                    }
//                }
//
//
//            } catch (SAXException e1) {
//            } catch (IOException e1) {
//            } catch (ParserConfigurationException e1) {
//            }
    }

    @Nullable
    @Override
    public String getTitle() {
        return "Override";  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void handleMnemonic(KeyEvent keyEvent, Project project, JBPopup jbPopup) {
    }

    @Nullable
    @Override
    public JComponent createAccessoryView(Project project) {
        return null;
    }

    @Override
    public Object[] getSelectedItemsInTree() {
        return new Object[0];
    }

    @Override
    public void itemChosen(ItemWrapper itemWrapper, Project project, JBPopup jbPopup, boolean b) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void removeSelectedItemsInTree() {
        //To change body of implemented methods use File | Settings | File Templates.
    }


//        LocalFileSystem.getInstance().findFileByIoFile

}
