package com.kodokux.mageidea.util;

import org.apache.tools.tar.TarEntry;
import org.apache.tools.tar.TarOutputStream;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.zip.GZIPOutputStream;

/**
 * User: johna1203
 * Date: 2013/10/13
 * Time: 5:07
 */
public class TgzCompressor {

    private File srcFile;
    private File dstFile;

    public void setSrcFile(File file) {
        srcFile = file;
    }

    public void setDstFile(File file) {
        dstFile = file;
    }

    public void save() throws IOException {
        // write tar+gz
        File dstfile = dstFile;
        final TarOutputStream tos = new TarOutputStream(new GZIPOutputStream(new FileOutputStream(dstfile)));
        try {
            visitChildrenRecursively(srcFile, new RecursivelyListener() {
                @Override
                public void visited(File file) {
                    if (file.isFile()) {
                        String relativePath = com.intellij.openapi.util.io.FileUtil.getRelativePath(srcFile, file);
                        TarEntry entry = new TarEntry(file);
                        entry.setName(relativePath);


                        try {
                            tos.putNextEntry(entry);
                            InputStream is = new FileInputStream(file);
                            try {
                                transfer(is, tos);
                            } finally {
                                is.close();
                            }
                            tos.closeEntry();
                        } catch (IOException e) {
                        }
                    }
                }
            });
        } finally {
            tos.close();
        }
    }

    static void transfer(InputStream is, OutputStream os) throws IOException {
        byte[] buffer = new byte[8192]; // 何故8192なのでしょうか
        for (int readLength; (readLength = is.read(buffer)) >= 0; ) {
            os.write(buffer, 0, readLength);
        }
        os.flush();
    }

    interface RecursivelyListener {
        void visited(File file);
    }

    public void visitChildrenRecursively(@NotNull File dir, RecursivelyListener listener) {
        final File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {

                if (dstFile.getName().equals(file.getName())) {
                    continue;
                }


                if (file.getName().equals(".DS_Store") ||
                        file.getName().equals("modman") ||
                        file.getName().equals("genPackages") ||
                        file.getName().equals(".git") ||
                        file.getName().equals(".gitignore")) {
                    continue;
                }
                listener.visited(file);
                if (file.isDirectory()) {
                    visitChildrenRecursively(file, listener);
                }
            }
        }
    }
}
