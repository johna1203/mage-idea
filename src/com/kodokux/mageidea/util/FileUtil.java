package com.kodokux.mageidea.util;

import javax.annotation.processing.FilerException;
import java.io.*;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/10/09
 * Time: 17:23
 */
public class FileUtil {
    public static void displayDirectoryContents(File dir) {
        try {
            File[] files = dir.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    System.out.println("directory:" + file.getCanonicalPath());
                    displayDirectoryContents(file);
                } else {
                    System.out.println("     file:" + file.getCanonicalPath());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * ファイルのMD5を取得する
     *
     *
     * @param file MD5を取得するFile
     * @return MD5(Byte配列)
     * @throws FileNotFoundException ファイルが存在しない
     * @throws FilerException        指定したFileはDirectoryである。
     */
    public static String getFileMd5(File file) throws FileNotFoundException, FilerException {
        //ファイルのみ対応
        if (!file.exists()) throw new FileNotFoundException();
        if (file.isDirectory()) throw new FilerException(file.getAbsolutePath() + " is Directory!");

        //MD5計算
        DigestInputStream din = null;
        byte[] md5 = null;
        String str = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            din = new DigestInputStream(new BufferedInputStream(new FileInputStream(file)), md);

            //バッファに貯まるまで待機
            while (din.read() != -1) {
            }

            //MD5をbyte配列に格納
            byte[] dig = md.digest();

            for (byte b : dig) {
                str += String.format("%02x", b);
            }


            md5 = dig;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (din != null) {
                try {
                    din.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return str;
    }
}
