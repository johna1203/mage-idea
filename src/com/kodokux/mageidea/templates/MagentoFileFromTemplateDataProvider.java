package com.kodokux.mageidea.templates;

import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.kodokux.mageidea.xml.ConfigXmlUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Properties;

/**
 * User: johna1203
 * Date: 2013/09/24
 * Time: 2:31
 */
public interface MagentoFileFromTemplateDataProvider {
    @NotNull
    String getTemplateName();

    @NotNull
    String getFilePath();

    @NotNull
    PsiDirectory getBaseDirectory();

    @NotNull
    Properties getProperties(@NotNull com.intellij.psi.PsiDirectory psiDirectory);

    VirtualFile getConfigXml();

    ConfigXmlUtil.Rewrite getRewrite();

}
