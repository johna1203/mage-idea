package com.kodokux.mageidea.modman;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.CheckoutProvider;
import com.intellij.openapi.vcs.VcsKey;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.kodokux.mageidea.Settings;
import git4idea.checkout.GitCheckoutProvider;
import git4idea.commands.Git;

import java.io.File;

/**
 * User: johna1203
 * Date: 2013/10/04
 * Time: 1:09
 */
public class CloneAction extends AnAction {
    public void actionPerformed(AnActionEvent e) {

        final Project project = e.getProject();
        if (project == null)
            return;


        CloneDialog dialog = new CloneDialog(project);
        dialog.show();

        if (dialog.isOK()) {

            File destinationParentFile = new File(project.getBasePath());


            final VirtualFile destinationParent = LocalFileSystem.getInstance().findFileByIoFile(destinationParentFile);
            if (destinationParent == null) {
                return;
            }


            final String sourceRepositoryURL = dialog.getSourceRepositoryURL();
            String directoryName = sourceRepositoryURL.substring(sourceRepositoryURL.lastIndexOf('/')).replace(".git", "");

            if (directoryName.isEmpty()) {
                return;
            }

            directoryName = ".modman/" + directoryName;

            final String parentDirectory = destinationParentFile.getPath();
//
            Git git = ServiceManager.getService(Git.class);
            final String finalDirectoryName = directoryName;
            GitCheckoutProvider.clone(project, git, new CheckoutProvider.Listener() {
                @Override
                public void directoryCheckedOut(File file, VcsKey vcsKey) {

                }

                @Override
                public void checkoutCompleted() {

//                    File fileNative = new File(finalDirectoryName);
//                    LocalFileSystem.getInstance().refreshAndFindFileByIoFile(fileNative);
//                    File modmanNativeFile = new File(fileNative, "modman");
//                    ModmanFile modmanFile = new ModmanFile(project, fileNative, modmanNativeFile);
//                    List<String> links = modmanFile.getLinks();
//                    //ModmanUtils.updateModmanFile(myProject, fileNative, links);
//                    ModmanUtils.createSymbolicLinks(project, fileNative, links);


                }
            }, destinationParent, sourceRepositoryURL, directoryName, parentDirectory);

        }


//        Settings settings = Settings.getInstance();
//
//        if (settings.modmanPath.isEmpty()) {
//            VirtualFile file = ModmanUtils.downloadModman(project, null, project.getBasePath());
//            boolean b = new File(file.getPath()).setExecutable(true);
//            settings.modmanPath = file.getPath();
//            return;
//        }
//
//        CloneDialog cloneDialog = new CloneDialog(project);
//
//        cloneDialog.show();
//
//        if (cloneDialog.isOK()) {
//            System.out.println("OK");
//        }
        //GitCheckoutProvider.clone(project, git, listener, destinationParent, sourceRepositoryURL, directoryName, parentDirectory);


    }

    @Override
    public void update(AnActionEvent e) {

        Settings settings = Settings.getInstance();

        if (settings.modmanPath == null) {

        }
    }
}
