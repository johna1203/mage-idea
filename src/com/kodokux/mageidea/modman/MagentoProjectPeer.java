package com.kodokux.mageidea.modman;

import com.intellij.ide.util.projectWizard.SettingsStep;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.platform.WebProjectGenerator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * User: johna1203
 * Date: 2013/10/04
 * Time: 1:53
 */
public class MagentoProjectPeer implements WebProjectGenerator.GeneratorPeer {


    private MagentoProjectGenerateForm myForm = new MagentoProjectGenerateForm();

    @NotNull
    @Override
    public JComponent getComponent() {
        return myForm.getMainContenPanel();
    }

    @Override
    public void buildUI(@NotNull SettingsStep settingsStep) {
        settingsStep.addSettingsComponent(myForm.getMainContenPanel());
    }

    @NotNull
    @Override
    public Object getSettings() {
        return null;
    }

    @Nullable
    @Override
    public ValidationInfo validate() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isBackgroundJobRunning() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void addSettingsStateListener(@NotNull WebProjectGenerator.SettingsStateListener settingsStateListener) {
        myForm.addSettingsStateListener(settingsStateListener);
    }
}
