package com.kodokux.mageidea.modman;

import com.intellij.notification.NotificationType;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.util.io.FileUtilRt;
import com.intellij.openapi.vcs.ProjectLevelVcsManager;
import com.intellij.openapi.vcs.VcsDirectoryMapping;
import com.intellij.openapi.vcs.VcsException;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileVisitor;
import com.intellij.vcsUtil.VcsFileUtil;
import com.jetbrains.php.util.PhpConfigurationUtil;
import com.kodokux.mageidea.MagentoComponent;
import git4idea.GitVcs;
import git4idea.actions.GitInit;
import git4idea.commands.GitCommand;
import git4idea.commands.GitSimpleHandler;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: johna1203
 * Date: 2013/10/04
 * Time: 1:35
 */
public class ModmanUtils {
    static public VirtualFile downloadModman(Project project, JComponent component, String toDir) {
        return PhpConfigurationUtil.downloadFile(project, component, toDir, "https://raw.github.com/Kodokux/modman/master/modman", "modman");
    }

    public static void refreshAndConfigureVcsMappings(final Project project, final VirtualFile root, final String path) {
        GitInit.refreshAndConfigureVcsMappings(project, root, path);
    }

    public static void refreshAndRemoveVcsMappings(final Project project, final VirtualFile root, final String path) {
        root.refresh(false, false);
        ProjectLevelVcsManager vcs = ProjectLevelVcsManager.getInstance(project);
        final List<VcsDirectoryMapping> vcsDirectoryMappings = new ArrayList<VcsDirectoryMapping>(vcs.getDirectoryMappings());
        VcsDirectoryMapping mapping = new VcsDirectoryMapping(path, GitVcs.getInstance(project).getName());
        boolean hasMapping = false;
        for (int i = 0; i < vcsDirectoryMappings.size(); i++) {
            final VcsDirectoryMapping m = vcsDirectoryMappings.get(i);
            if (m.getDirectory().equals(path)) {
                hasMapping = true;
                break;
            }
        }
        if (hasMapping) {
            vcsDirectoryMappings.remove(mapping);
        }
        vcs.setDirectoryMappings(vcsDirectoryMappings);
        vcs.updateActiveVcss();
        VcsFileUtil.refreshFiles(project, Collections.singleton(root));
    }

    public static void remoteAddOrigin(final Project project, VirtualFile root, String url) throws VcsException {
        GitSimpleHandler handler = new GitSimpleHandler(project, root, GitCommand.REMOTE);
        handler.addParameters("add", "origin", url);
        handler.endOptions();
        handler.run();
    }

    public static void updateModmanFile(Project project, String rootPath) {
        File file = new File(rootPath);
        if (!file.getName().equals(".modman")) {
            VirtualFile virtualFile = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(file);
            VirtualFile modman = null;
            if (virtualFile != null && virtualFile.isDirectory()) {
                modman = virtualFile.findChild("modman");
                if (modman == null || !modman.exists()) {
                    File modman1 = new File(file, "modman");
                    try {
                        if (!modman1.createNewFile()) {
                            return;
                        } else {
                            modman = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(modman1);
                        }
                    } catch (IOException e) {
                        return;
                    }
                }
            }

            if (modman != null) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer = displayDirectoryContents(new File(rootPath), new File(modman.getParent().getPath()), stringBuffer);
                final StringBuffer finalStringBuffer = stringBuffer;
                final VirtualFile finalModman = modman;
                ApplicationManager.getApplication().runWriteAction(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            finalModman.setBinaryContent(finalStringBuffer.toString().getBytes(Charset.forName("UTF8")));
                        } catch (IOException e) {
                        }
                    }
                });
            }
        }
    }

    public static StringBuffer displayDirectoryContents(File dir, File rootPath, StringBuffer buffer) {

        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.getName().equals(".git")
                    || file.getName().equals("modman")
                    || file.getName().equals(".gitignore"))
                continue;

            if (file.isDirectory()) {
                buffer = displayDirectoryContents(file, rootPath, buffer);
            } else {
                String relativePath = FileUtilRt.getRelativePath(rootPath, file);
                buffer.append(relativePath + "  " + relativePath);
                buffer.append("\n");
            }
        }

        return buffer;
    }

    public static void updateModmanFile(Project myProject, File root, final List<String> links) {

        if (root.getParentFile().getName().equals(".modman")) {
            File modmanFile = new File(root, "modman");
            if (!modmanFile.exists()) {
                try {
                    modmanFile.createNewFile();
                } catch (IOException e) {
                }
            }

            final VirtualFile modmanVirtualFile = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(modmanFile);
            if (modmanVirtualFile != null) {

                final StringBuffer stringBuffer = new StringBuffer();
                for (String link : links) {
                    stringBuffer.append(link + "\t" + link);
                    stringBuffer.append("\n");
                }


                ApplicationManager.getApplication().runWriteAction(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            byte[] utf8s = stringBuffer.toString().getBytes(Charset.forName("UTF8"));
                            modmanVirtualFile.setBinaryContent(utf8s);
                        } catch (IOException e) {
                        }
                    }
                });
            }
        }
    }

    public static void createSymbolicLinks(Project myProject, File root, List<String> links) {
        for (String link : links) {
            File file = new File(myProject.getBasePath(), link);
            File base = new File(root.getPath(), link);
            String relativePath = FileUtil.getRelativePath(file, base);
//            System.out.println(relativePath);

            try {
                Path symbolicLink = Files.createSymbolicLink(Paths.get(file.getPath()), Paths.get(relativePath));
                MagentoComponent.showInLogEvent("Success", "Create Symbolic Link " + symbolicLink.toString());
                LocalFileSystem.getInstance().refreshAndFindFileByIoFile(file);
            } catch (FileAlreadyExistsException e) {
//                Notifications.Bus.notify(new Notification("Mageman", "Info", e.getMessage(), NotificationType.INFORMATION));
            } catch (IOException e) {
                MagentoComponent.showInLogEvent("Error", "Error log ", NotificationType.ERROR);
            }
        }
    }

    public static void deleteSymbolicLinks(Project myProject, VirtualFile file, String relativePath) {
        if (relativePath != null) {

            String rootPath = null;
            if (file.getParent().getName().equals(".modman")) {
                rootPath = file.getPath();
            } else {
                String substring = relativePath.substring(0, relativePath.indexOf('/'));
                File tmp = new File(myProject.getBasePath() + "/.modman/" + substring);
                rootPath = tmp.getPath();
            }


            final String projectRoot = myProject.getBasePath();
            final String extensiotnRoot = rootPath;
            VfsUtil.visitChildrenRecursively(file, new VirtualFileVisitor() {

                @Override
                public void afterChildrenVisited(@NotNull VirtualFile file) {
                    String relativePath2 = FileUtil.getRelativePath(extensiotnRoot, file.getPath(), '/');
                    File inMagentoFile = new File(projectRoot, relativePath2);
                    if (Files.isSymbolicLink(Paths.get(inMagentoFile.getPath()))) {
                        VirtualFile ioFile = LocalFileSystem.getInstance().refreshAndFindFileByPath(inMagentoFile.getPath());
                        if (ioFile != null) {
                            String canonicalPath = ioFile.getCanonicalPath();
//                            if (canonicalPath != null && canonicalPath.equals(file.getPath())) {
                                if (FileUtil.delete(inMagentoFile)) {
                                    MagentoComponent.showInLogEvent("Success", "Delete Symbolic Link" + inMagentoFile.getPath());
                                    ioFile.refresh(false, false);
                                } else {
                                    MagentoComponent.showInLogEvent("Error", "Cant delete : " + inMagentoFile.getPath(), NotificationType.ERROR);
                                }
//                            } else {
//
//                            }
                        }
                    }
                }

                @Override
                public boolean visitFile(@NotNull VirtualFile file) {
                    if (file.getName().equals(".git") && file.getName().equals("modman")) {
                        return false;
                    }
                    return true;
                }
            });
//                if (Files.isSymbolicLink(Paths.get(deletedFile.getPath()))) {
//                    String canonicalPath = ioFile.getCanonicalPath();
//                    if (canonicalPath != null && canonicalPath.equals(file.getPath())) {
//                        if (deletedFile.delete()) {
//                            MagentoComponent.showInLogEvent("Success", "Delete Symbolic Link" + deletedFile.getPath());
//                            ioFile.refresh(false, false);
//                        } else {
//                            MagentoComponent.showInLogEvent("Error", "Cant remove Symbolic Link", NotificationType.ERROR);
//                        }
//                    }
//                }
        }
//        }
    }

    private static void deleteDirIsEmpty(VirtualFile parentDir) {
        VirtualFile[] children = parentDir.getChildren();
        if (children.length == 0) {
//            try {
            if (FileUtil.delete(new File(parentDir.getPath()))) {
                parentDir.refresh(false, false);
            } else {
                MagentoComponent.showInLogEvent("Error", "Cant delete : " + parentDir.getPath(), NotificationType.ERROR);
            }
//            } catch (IOException e) {
//            }
        }
    }


}
