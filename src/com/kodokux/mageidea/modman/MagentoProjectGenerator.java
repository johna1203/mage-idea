package com.kodokux.mageidea.modman;

import com.intellij.ide.util.projectWizard.WebProjectTemplate;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

/**
 * User: johna1203
 * Date: 2013/10/04
 * Time: 1:48
 */
public class MagentoProjectGenerator extends WebProjectTemplate {
    @Nls
    @NotNull
    @Override
    public String getName() {
        return "Magento";  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getDescription() {
        return "Magento Extension";  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void generateProject(@NotNull Project project, @NotNull VirtualFile virtualFile, @NotNull Object o, @NotNull Module module) {
        System.out.println("generateProject");
    }

    @NotNull
    @Override
    public GeneratorPeer createPeer() {
        return new MagentoProjectPeer();
    }
}
