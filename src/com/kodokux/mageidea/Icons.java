package com.kodokux.mageidea;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/13
 * Time: 15:00
 */
public class Icons {
    public static Icon MAGENTO_ICON_16 = IconLoader.findIcon("/icon16.png");
    public static Icon MAGENTO_ICON_16_ON = IconLoader.findIcon("/icon16On.png");
}
