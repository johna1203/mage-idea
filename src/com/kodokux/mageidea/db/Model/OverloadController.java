package com.kodokux.mageidea.db.Model;

import java.sql.*;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/23
 * Time: 21:45
 */
public class OverloadController extends AbstractModel {

    static public String TABLE_NAME = "extension_overload_controller";

    public OverloadController(Connection connection) {
        super(connection);
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    public int insertOrSave(String extensionId,
                            String overloadType,
                            String routeName,
                            String nameSpace,
                            String before,
                            String rewriteClass) {
        try {
            PreparedStatement preparedStatement =
                    getConnection().prepareStatement(
                            "SELECT * FROM " + getTableName() +
                                    " WHERE extensionId = ? " +
                                    "AND overloadType = ? " +
                                    "AND routeName = ? " +
                                    "AND nameSpace = ?"
                    );
            getConnection().setAutoCommit(false);
            preparedStatement.setString(1, extensionId);
            preparedStatement.setString(2, overloadType);
            preparedStatement.setString(3, routeName);
            preparedStatement.setString(3, nameSpace);
            ResultSet result = preparedStatement.executeQuery();

            int id = 0;
            if (result.next()) {
                id = result.getInt(1);
                PreparedStatement insertStatement =
                        getConnection().prepareStatement("UPDATE " + getTableName() + " SET " +
                                "extensionId = ?, " +
                                "overloadType = ?, " +
                                "routeName = ?, " +
                                "nameSpace = ?," +
                                "before = ?" +
                                "rewriteClass = ?" +
                                "where id = ?");
                insertStatement.setString(1, extensionId);
                insertStatement.setString(2, overloadType);
                insertStatement.setString(3, routeName);
                insertStatement.setString(4, nameSpace);
                insertStatement.setString(5, rewriteClass);
                insertStatement.setInt(6, id);
                insertStatement.executeUpdate();
            } else {
                PreparedStatement insertStatement =
                        getConnection().prepareStatement("INSERT INTO  " + getTableName() + " values(" +
                                "null, ?, ?, ?, ?, ?, ?)",
                                Statement.RETURN_GENERATED_KEYS);
                insertStatement.setString(1, extensionId);
                insertStatement.setString(2, overloadType);
                insertStatement.setString(3, routeName);
                insertStatement.setString(4, nameSpace);
                insertStatement.setString(5, before);
                insertStatement.setString(6, rewriteClass);
                insertStatement.executeUpdate();
                ResultSet generatedKeys = insertStatement.getGeneratedKeys();
                generatedKeys.next();
                id = generatedKeys.getInt(1);
            }
            getConnection().commit();

            return id;

        } catch (SQLException e) {
            try {
                getConnection().rollback();
            } catch (SQLException e1) {
            }
            e.printStackTrace();
            return 0;
        }
    }

}
