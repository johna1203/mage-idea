package com.kodokux.mageidea.db;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.CommandProcessor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.Ref;
import com.intellij.util.IncorrectOperationException;
import com.kodokux.mageidea.db.Model.Event;
import com.kodokux.mageidea.db.Model.Extension;
import com.kodokux.mageidea.db.Model.OverloadController;
import com.kodokux.mageidea.db.Model.Rewrite;

import java.io.File;
import java.sql.*;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/16
 * Time: 14:13
 */
public class MyConnection {

    private static MyConnection INSTANCE;
    final private Project myProject;
    final private String myDbname = "mageidea.db";
    final private String baseDirPath;
    final private String configDirPath;

    private Connection connection;

    public MyConnection(Project project) {

        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
        }

        myProject = project;
        baseDirPath = project.getBasePath() + File.separator + ".idea";
        configDirPath = project.getBasePath() + File.separator + ".idea" + File.separator + "mage-idea";


        init();
    }

    public static MyConnection getInstance(Project project) {
        if (INSTANCE == null) {
            INSTANCE = new MyConnection(project);
        }
        return INSTANCE;
    }

    private void init() {

        final Ref fileRef = new Ref();
        final Ref exceptionRef = new Ref();
        final String dbFullPath = configDirPath + File.separator + myDbname;
        CommandProcessor.getInstance().executeCommand(myProject, new Runnable() {
            @Override
            public void run() {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
//                        fileRef.set(PhpFileTemplateUtil.createPhpFileFromInternalTemplate(project, baseDir, templateName, properties, fileName));
                        try {
                            Class.forName("org.sqlite.JDBC");
                            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + dbFullPath);
                            Statement statement = connection.createStatement();
                            statement.setQueryTimeout(30);
                            createTables(statement);
                            fileRef.set(connection);
                        } catch (ClassNotFoundException e) {
                            exceptionRef.set(new IncorrectOperationException("No load org.sqlite.JDBC"));
                            return;
                        } catch (SQLException e) {
                            exceptionRef.set(new IncorrectOperationException("Cant connect database"));
                            return;
                        }
                    }
                };

                ApplicationManager.getApplication().runReadAction(runnable);
            }
        }, "Init DB", null);

        if (!exceptionRef.isNull()) {
            Messages.showErrorDialog(((IncorrectOperationException) exceptionRef.get()).getMessage(), "Init DB");
            return;
        }

        if (fileRef.isNull())
            return;

        connection = (Connection) fileRef.get();
    }

    public Connection getConnection() {
        return connection;
    }

    public void rebaseDB() throws SQLException {
        Statement statement = getConnection().createStatement();
        statement.execute("DROP TABLE IF EXISTS " + Extension.TABLE_NAME);
        statement.execute("DROP TABLE IF EXISTS " + Event.TABLE_NAME);
        statement.execute("DROP TABLE IF EXISTS " + Rewrite.TABLE_NAME);
        statement.execute("DROP TABLE IF EXISTS " + OverloadController.TABLE_NAME);
        createTables(statement);
    }

    private void createTables(Statement statement) throws SQLException {
        //create table
        statement.executeUpdate("create table IF NOT EXISTS " + Extension.TABLE_NAME + " (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "extensionName string," +
                "version string," +
                "channel string," +
                "fullPath text" +
                ")");

        statement.executeUpdate("create table IF NOT EXISTS " + Event.TABLE_NAME + " (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "extensionId string," +
                "eventName string," +
                "eventType string," +
                "className text," +
                "methodName text" +
                ")");

        statement.executeUpdate("create table IF NOT EXISTS " + Rewrite.TABLE_NAME + " (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "extensionId string," +
                "rewriteType string," +
                "tagName string," +
                "rewriteClassShortName text," +
                "rewriteClass string" +
                ")");

        statement.executeUpdate("create table IF NOT EXISTS " + OverloadController.TABLE_NAME + " (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "extensionId string," +
                "overloadType string," +
                "routeName string," +
                "nameSpace string," +
                "before string," +
                "rewriteClass string" +
                ")");

    }

    public void show() throws SQLException {
        Statement statement = getConnection().createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM extensions");
        while (rs.next()) {
            System.out.println("extensionName = " + rs.getString("extensionName"));
        }
    }
}
