package com.kodokux.mageidea.magerun;

import com.intellij.openapi.project.Project;
import com.jetbrains.php.macro.PhpExecutableMacro;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/10/01
 * Time: 17:23
 */
public class MagerunUtils {

    public static String getMagerunCommad(Project project) {
        List commands = new ArrayList();
        String phpCommand = PhpExecutableMacro.getPhpCommand(project);
        commands.add(phpCommand);
        commands.add("/usr/local/bin/n98-magerun.phar");
        return phpCommand + " /usr/local/bin/n98-magerun.phar";
    }
}
