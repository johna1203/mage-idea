package com.kodokux.mageidea.service;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/13
 * Time: 15:51
 */
public class ConfigUtil {

    private static ConfigUtil INSTANCE = null;
    private final Project project;
    private Document document;

    public static ConfigUtil getInstance(Project project) {
        if (INSTANCE == null)
            INSTANCE = new ConfigUtil(project);
        return INSTANCE;
    }

    public ConfigUtil(Project project) {
        this.project = project;
        FileSystem fileSystem = new FileSystem(project);
        File file = fileSystem.getFile("config.xml");
        try {
            document = getDocument(file);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    private XPath getXpath() {
        XPathFactory xpfactory = XPathFactory.newInstance();
        return xpfactory.newXPath();
    }

    private Object evaluate(Document document, String path, QName type) {
        XPathExpression expr = null;
        try {
            expr = getXpath().compile(path);
            return expr.evaluate(document, type);
        } catch (XPathExpressionException e) {
        }
        return null;
    }

    private Document getDocument(File file) throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        return dBuilder.parse(file);
    }

    public Document getDocument() {
        return document;
    }


    public List<String> getExtensionList() {
        Document document1 = getDocument();
        NodeList evaluate = (NodeList) evaluate(document1, "/config/modules", XPathConstants.NODESET);

        List<String> list = new ArrayList<String>();
        if (evaluate.getLength() > 0) {
            for (int i = 0; i < evaluate.getLength(); i++) {
                Node modules = evaluate.item(i).getFirstChild();
                String extensionName = "";
                if (modules.getNodeType() == Node.TEXT_NODE) {
                    Node nextSibling = modules.getNextSibling();
                    extensionName = nextSibling.getNodeName();
                } else {
                    extensionName = modules.getNodeName();
                }
                list.add(extensionName);
            }
        }
        Collections.sort(list);

        return list;
    }
}