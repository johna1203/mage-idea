package com.kodokux.mageidea.service;

import com.intellij.CommonBundle;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Computable;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileManager;
import com.sun.org.apache.xml.internal.serializer.OutputPropertiesFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.w3c.dom.Document;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * User: johna1203
 * Date: 2013/09/15
 * Time: 16:31
 */
public class FileSystem {
    private static final Logger LOG = Logger.getInstance(FileSystem.class);
    private final String confingPath;
    private final VirtualFileManager fileManager = VirtualFileManager.getInstance();
    private static final String MAGE_IDEA_FOLDER = "";
    private static final Charset CHARSET = Charset.forName("UTF8");


    public static FileSystem getInstance(Project project) {
        return ServiceManager.getService(project, FileSystem.class);
    }

    public FileSystem(@NotNull Project project) {
        this.confingPath = project.getBasePath() + "/.idea/magento-idea";
    }

    public boolean createEmptyFile(String fileName) {
        return createFile(fileName, "");
    }

    public boolean createFile(final String fileName, final String text) {
        return ApplicationManager.getApplication().runWriteAction(new Computable<Boolean>() {
            @Override
            public Boolean compute() {
                try {
                    ensureExists(new File(confingPath));

                    VirtualFile configFolder = virtualFileBy(MAGE_IDEA_FOLDER);
                    if (configFolder == null) return false;

                    VirtualFile configFile = configFolder.createChildData(FileSystem.this, fileName);
//                    configFile.setBinaryContent(text.getBytes(CHARSET));

                    return true;
                } catch (IOException e) {
                    LOG.warn(e);
                    return false;
                }
            }
        });
    }

    @Nullable
    public VirtualFile virtualFileBy(String fileName) {
        return fileManager.refreshAndFindFileByUrl("file://" + confingPath + fileName);
    }

    private static void ensureExists(File dir) throws IOException {
        if (!dir.exists() && !dir.mkdirs()) {
            throw new IOException(CommonBundle.message("exception.directory.can.not.create", dir.getPath()));
        }
    }

    public boolean hasFile(String s) {
        File file = new File(confingPath, s);
        return file.exists();
    }

    public File getFile(String s) {
        File file = new File(confingPath, s);
        if (file.exists())
            return file;

        return null;
    }

    public Boolean createFile(final String fileName, final Document merge) {
        return ApplicationManager.getApplication().runWriteAction(new Computable<Boolean>() {
            @Override
            public Boolean compute() {
                Transformer transformer = null;
                try {
                    transformer = TransformerFactory.newInstance().newTransformer();
                    Result output = new StreamResult(new File(fileName));
                    Source input = new DOMSource(merge);
                    transformer.transform(input, output);
                } catch (TransformerConfigurationException e) {
                    return false;
                } catch (TransformerException e) {
                    return false;
                }
                return true;
            }
        });
    }

    public Boolean createFile(final File file, final Document merge) {
        return ApplicationManager.getApplication().runWriteAction(new Computable<Boolean>() {
            @Override
            public Boolean compute() {
                Transformer transformer = null;
                try {
                    transformer = TransformerFactory.newInstance().newTransformer();
                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                    transformer.setOutputProperty(OutputPropertiesFactory.S_KEY_INDENT_AMOUNT, "4");

                    Result output = new StreamResult(file);
                    Source input = new DOMSource(merge);
                    transformer.transform(input, output);
                } catch (TransformerConfigurationException e) {
                    return false;
                } catch (TransformerException e) {
                    return false;
                }
                return true;
            }
        });
    }
}
