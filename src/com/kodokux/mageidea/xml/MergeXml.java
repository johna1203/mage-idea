package com.kodokux.mageidea.xml;

import com.intellij.openapi.components.StorageScheme;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;

/**
 * User: johna1203
 * Date: 2013/09/14
 * Time: 20:13
 */
public class MergeXml {

    public static void main(String[] args) throws Exception {
        // proper error/exception handling omitted for brevity
////        org.dom4j.Document base = reader.read("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/app/code/core/Mage/Tag/etc/config.xml");
/////        org.dom4j.Document merge = reader.read("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/app/code/core/Mage/Cms/etc/config.xml");
        File file1 = new File("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/app/code/core/Mage/Tag/etc/config.xml");
        File file2 = new File("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/app/code/core/Mage/Cms/etc/config.xml");
        Document doc = merge("/config", file1, file2);
        print(doc);

        XPathExpression xPathExpression = getxPathExpression("/config/global//blocks");
        NodeList results = (NodeList) xPathExpression.evaluate(doc,
                XPathConstants.NODESET);

        if (results.getLength() > 0) {
            for (int i = 0; i < results.getLength(); i++) {
                Node item = results.item(i);
                if (item.getNodeType() == Node.ELEMENT_NODE)
                    System.out.println(item.getNodeName());
            }
        } else {
            System.out.println("No node");
        }

    }

    public static Document merge(String expression,
                                 File... files) throws Exception {
        XPathExpression compiledExpression = getxPathExpression(expression);
        return merge(compiledExpression, files);
    }

    private static XPathExpression getxPathExpression(String expression) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xpath = xPathFactory.newXPath();
        return xpath
                .compile(expression);
    }

    private static Document merge(XPathExpression expression,
                                  File... files) throws Exception {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
                .newInstance();
        docBuilderFactory
                .setIgnoringElementContentWhitespace(true);
        DocumentBuilder docBuilder = docBuilderFactory
                .newDocumentBuilder();
        Document base = docBuilder.parse(files[0]);

        Node results = (Node) expression.evaluate(base,
                XPathConstants.NODE);
        if (results == null) {
            throw new IOException(files[0]
                    + ": expression does not evaluate to node");
        }

        for (int i = 1; i < files.length; i++) {
            if (files[i] == null) {
                continue;
            }
            Document merge = docBuilder.parse(files[i]);
            Node nextResults = (Node) expression.evaluate(merge,
                    XPathConstants.NODE);
            while (nextResults.hasChildNodes()) {
                Node kid = nextResults.getFirstChild();
                nextResults.removeChild(kid);
                kid = base.importNode(kid, true);
                results.appendChild(kid);
            }
        }

        return base;
    }

    private static void print(Document doc) throws Exception {
        TransformerFactory transformerFactory = TransformerFactory
                .newInstance();
        Transformer transformer = transformerFactory
                .newTransformer();
        DOMSource source = new DOMSource(doc);
        Result result = new StreamResult(System.out);
        transformer.transform(source, result);
    }

//    public static void main(String[] args) throws Exception {
//        // proper error/exception handling omitted for brevity
//
////        SAXReader reader = new SAXReader();
//
////        org.dom4j.Document base = reader.read("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/app/code/core/Mage/Tag/etc/config.xml");
/////        org.dom4j.Document merge = reader.read("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/app/code/core/Mage/Cms/etc/config.xml");
//
//        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
//                .newInstance();
//        docBuilderFactory
//                .setIgnoringElementContentWhitespace(true);
//        DocumentBuilder docBuilder = docBuilderFactory
//                .newDocumentBuilder();
//        Document base = docBuilder.parse("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/app/code/core/Mage/Tag/etc/config.xml");
//
//        Document merge = docBuilder.parse("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/app/code/core/Mage/Cms/etc/config.xml");
////
////
////        String[] expressionsString = {
////                "/config/modules",
////                "/config/global/models",
////                "/config/global/resources",
////                "/config/global/index",
////
////                "/config/frontend/routers",
////                "/config/frontend/translate/modules",
////                "/config/frontend/layout/updates",
////
////        };
////
////        XPathExpression[] expressions = new XPathExpression[expressionsString.length];
////
////        for (int i = 0; i < expressionsString.length; i++) {
////            expressions[i] = getxPathExpression(expressionsString[i]);
////        }
////
////
////        Document doc = merge(base, merge, expressions);
////        print(doc);
//        Document parse = parse(base, base.getDocumentElement(), merge, "");
//        print(parse);
//    }
//
//    private static Document parse(final Document base, final Element baseEl, Document merge, String level) throws XPathExpressionException {
//        final NodeList children = baseEl.getChildNodes();
//        level += "/" + baseEl.getNodeName();
//        for (int i = 0; i < children.getLength(); i++) {
//            final Node n = children.item(i);
//            if (n.getNodeType() == Node.ELEMENT_NODE) {
//                if (!level.equals("/config")) {
//                    XPathExpression xPathExpression = getxPathExpression(level);
//                    Node nextResults = (Node) xPathExpression.evaluate(merge, XPathConstants.NODE);
//
//                    if (nextResults != null) {
////                        System.out.println(level + "::" + n.getNodeName());
//
//                        //次あるかチェック
//                        if (n.hasChildNodes()) {
//                            NodeList childNodes = n.getChildNodes();
//                            for (int j = 0; j < childNodes.getLength(); j++) {
//                                if (childNodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
//                                    String level2 = level + "/" + childNodes.item(j).getNodeName();
//                                    XPathExpression xPathExpression2 = getxPathExpression(level2);
//                                    Node merge_childNode = (Node) xPathExpression2.evaluate(merge, XPathConstants.NODE);
//                                    if (merge_childNode == null || !merge_childNode.hasChildNodes()) {
//                                        while (nextResults.hasChildNodes()) {
//                                            Node kid = nextResults.getFirstChild();
//                                            nextResults.removeChild(kid);
//                                            if (kid.getNodeType() == Node.ELEMENT_NODE) {
//                                                System.out.println(level2);
//                                                System.out.println(kid.getNodeName());
//                                                System.out.println(n.getNodeName());
//                                                System.out.println("-------------------------------");
//                                                kid = base.importNode(kid, true);
//                                                n.getParentNode().appendChild(kid);
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//
//
////                        continue;
//                    }
//                }
//
//                parse(base, (Element) n, merge, level);
//            }
//        }
//        return base;
//    }
//
//
//    private static Document merge(String expression,
//                                  File... files) throws Exception {
//        XPathExpression compiledExpression = getxPathExpression(expression);
//        return merge(compiledExpression, files);
//    }
//
//    private static XPathExpression getxPathExpression(String expression) throws XPathExpressionException {
//        XPathFactory xPathFactory = XPathFactory.newInstance();
//        XPath xpath = xPathFactory.newXPath();
//        return xpath
//                .compile(expression);
//    }
//
//    private static Document merge(XPathExpression expression,
//                                  File... files) throws Exception {
//        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
//                .newInstance();
//        docBuilderFactory
//                .setIgnoringElementContentWhitespace(true);
//        DocumentBuilder docBuilder = docBuilderFactory
//                .newDocumentBuilder();
//        Document base = docBuilder.parse(files[0]);
//
//        Node results = (Node) expression.evaluate(base,
//                XPathConstants.NODE);
//        if (results == null) {
//            throw new IOException(files[0]
//                    + ": expression does not evaluate to node");
//        }
//
//        for (int i = 1; i < files.length; i++) {
//            Document merge = docBuilder.parse(files[i]);
//            Node nextResults = (Node) expression.evaluate(merge,
//                    XPathConstants.NODE);
//            while (nextResults.hasChildNodes()) {
//                Node kid = nextResults.getFirstChild();
//                nextResults.removeChild(kid);
//                kid = base.importNode(kid, true);
//                results.appendChild(kid);
//            }
//        }
//
//        return base;
//    }
//
//    private static Document merge(Document base, Document merge, XPathExpression... expressions) throws Exception {
//        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
//                .newInstance();
//        docBuilderFactory
//                .setIgnoringElementContentWhitespace(true);
////        DocumentBuilder docBuilder = docBuilderFactory
////                .newDocumentBuilder();
////        Document base = docBuilder.parse(files[0]);
//
//        for (int i = 0; i < expressions.length; i++) {
//            XPathExpression expression = expressions[i];
//            Node results = (Node) expression.evaluate(base,
//                    XPathConstants.NODE);
//            if (results == null) {
//                throw new IOException(": expression does not evaluate to node");
//            }
//
//
//            Node nextResults = (Node) expression.evaluate(merge,
//                    XPathConstants.NODE);
//            if (nextResults != null) {
//                while (nextResults.hasChildNodes()) {
//                    Node kid = nextResults.getFirstChild();
//                    nextResults.removeChild(kid);
//                    kid = base.importNode(kid, true);
//                    results.appendChild(kid);
//                }
//            }
//        }
////        }
//
//        return base;
//    }
//
//
//    private static void print(Document doc) throws Exception {
//        TransformerFactory transformerFactory = TransformerFactory
//                .newInstance();
//        Transformer transformer = transformerFactory
//                .newTransformer();
//        DOMSource source = new DOMSource(doc);
//        Result result = new StreamResult(System.out);
//        transformer.transform(source, result);
//    }

}
