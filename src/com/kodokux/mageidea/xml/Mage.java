package com.kodokux.mageidea.xml;

import com.intellij.openapi.project.Project;

import java.io.File;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/30
 * Time: 19:04
 */
public class Mage {
    static Mage INSTANCE = null;
    final static String PS = File.pathSeparator;
    final String BP;

    public Mage getInstance(Project project) {
        if (INSTANCE == null) {
            INSTANCE = new Mage(project);
        }
        return INSTANCE;
    }

    public Mage(Project project) {
        BP = project.getBasePath();
    }
}
