package com.kodokux.mageidea.xml;

import com.intellij.openapi.util.io.FileUtil;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;
import org.jetbrains.annotations.NotNull;

import javax.annotation.processing.FilerException;
import java.io.*;
import java.util.*;

/**
 * User: johna1203
 * Date: 2013/10/11
 * Time: 1:06
 */
public class PackageXml {

    //    <name>ECGKodokux_Voucher</name>
    private String nameTag = "";
    //    <version>1.0.1.6</version>
    private String versionTag = "";
    //    <stability>stable</stability>
    private String stabilityTag = "";
    //    <license uri="http://opensource.org/licenses/BSD-2-Clause">BSD 2-Clause License</license>
    private String licenseTag = "";
    private String licenseUrlTag;
    //    <channel>community</channel>
    private String channelTag = "";
    //    <summary>ECGKodokux_Voucher</summary>
    private String summaryTag = "";
    //    <description>&#x51FA;&#x8377;&#x4F1D;&#x7968;&#x306E;&#x51FA;&#x529B;&#x5165;&#x529B;&#xFF01;</description>
    private String descriptionTag = "";
    //    <notes></notes>
    private String notesTag = "";
    private String dateTag = "";
    //    <time>15:32:01</time>
    private String timeTag = "";


    //    <authors>
    //      <author>
    //        <name>Johnathan David Froeming</name>
    //        <user>johna1203</user>
    //        <email>johnathan@kodokuman.com</email>
    //      </author>
    //    </authors>
    //    <extends/>
    private String extendsTag = "";
    private List<Author> authorsTag = new ArrayList<Author>();
    //    <date>2013-09-06</date>

    //<compatible/>
    private String compatibleTag = "";
    private String phpMaximum = "";
    private String phpMinimum = "5.3.1";
//    <dependencies>
//    <required>
//    <php>
//    <min>5.2.12</min>
//    <max>6.0.0</max>
//    </php>
//    <package>
//    <name>ECGKodokux_Common</name>
//    <channel>community</channel>
//    <min>0.9.0</min>
//    <max></max>
//    </package>
//    </required>
//    </dependencies>


    private HashMap<String, String> packageXpaths = new HashMap<String, String>();

    {
        packageXpaths.put("name", "/package/name");
        packageXpaths.put("version", "/package/version");
        packageXpaths.put("stability", "/package/stability");
        packageXpaths.put("license", "/package/license");
        packageXpaths.put("channel", "/package/channel");
        packageXpaths.put("summary", "/package/summary");
        packageXpaths.put("description", "/package/description");
        packageXpaths.put("notes", "/package/notes");
        packageXpaths.put("date", "/package/date");
        packageXpaths.put("time", "/package/time");
        packageXpaths.put("authors", "/package/authors");
        packageXpaths.put("phpmin", "/package/dependencies/required/php/min");
        packageXpaths.put("phpmax", "/package/dependencies/required/php/max");
    }


    private final Document document;
    final private String LOCAL_MODULE = "app/code/local";
    final private String COMMUNITY_MODULE = "app/code/community";
    final private String COMMUNITY_CORE = "app/code/core";
    final private String TEMPLATES_LAYOUT = "app/design";
    final private String GLOBAL_CONFIGURATION = "app/etc";
    final private String PHP_LIBRARY = "lib";
    final private String LOCALE_LANGUAGE = "app/locale";
    final private String MEDIA_LIBRARY = "media";
    final private String THEME_SKIN = "skin";
    final private String PHPUNIT_TEST = "tests";
    final private String OTHER_WEB = ".";
    final private String OTHER = ".";

    final private HashMap<String, String> ALL_PATH = new HashMap<String, String>();

    {
        ALL_PATH.put(LOCAL_MODULE, "magelocal");
        ALL_PATH.put(COMMUNITY_MODULE, "magecommunity");
        ALL_PATH.put(COMMUNITY_CORE, "magecore");
        ALL_PATH.put(TEMPLATES_LAYOUT, "magedesign");
        ALL_PATH.put(GLOBAL_CONFIGURATION, "mageetc");
        ALL_PATH.put(PHP_LIBRARY, "magelib");
        ALL_PATH.put(LOCALE_LANGUAGE, "magelocale");
        ALL_PATH.put(MEDIA_LIBRARY, "magemedia");
        ALL_PATH.put(THEME_SKIN, "mageskin");
        ALL_PATH.put(PHPUNIT_TEST, "magetests");
        ALL_PATH.put(OTHER_WEB, "mageweb");
        ALL_PATH.put(OTHER, "other");
    }

    private final File myRootFile;
    private final File myPackegeFile;


    public PackageXml(File rootFile, File packageFile) throws JDOMException, IOException {
        SAXBuilder builder = new SAXBuilder();
        if (!packageFile.exists()) {
            createPackageFile(packageFile);
        }


        document = builder.build(packageFile);
        myPackegeFile = packageFile;
        myRootFile = rootFile;
    }

    private void createPackageFile(File packageFile) {
        Element el = new Element("package");
        Document document1 = new Document();
        document1.addContent(el);
        try {
            save(document1, packageFile);
        } catch (IOException e) {
        }

    }

    public void loadXml() {

        //name
        Element el = getNode(packageXpaths.get("name"));
        if (el != null) {
            nameTag = el.getValue();
        }

        el = getNode(packageXpaths.get("version"));
        if (el != null) {
            versionTag = el.getValue();
        }

        el = getNode(packageXpaths.get("stability"));
        if (el != null) {
            stabilityTag = el.getValue();
        }

        el = getNode(packageXpaths.get("license"));
        if (el != null) {
            licenseTag = el.getValue();

            Attribute uri = el.getAttribute("uri");
            if (uri != null) {
                licenseUrlTag = uri.getValue();
            }

        }

        el = getNode(packageXpaths.get("channel"));
        if (el != null) {
            channelTag = el.getValue();
        }

        el = getNode(packageXpaths.get("summary"));
        if (el != null) {
            summaryTag = el.getValue();
        }

        el = getNode(packageXpaths.get("description"));
        if (el != null) {
            descriptionTag = el.getValue();
        }

        el = getNode(packageXpaths.get("notes"));
        if (el != null) {
            notesTag = el.getValue();
        }

        el = getNode(packageXpaths.get("date"));
        if (el != null) {
            dateTag = el.getValue();
        }

        el = getNode(packageXpaths.get("time"));
        if (el != null) {
            timeTag = el.getValue();
        }

        el = getNode(packageXpaths.get("authors"));
        if (el != null) {
            for (Element author : el.getChildren()) {
                if (author.getName().equals("author")) {
                    Author authorObj = new Author();
                    Element authorNameEl = author.getChild("name");
                    if (authorNameEl != null) {
                        authorObj.setName(authorNameEl.getValue());
                    }

                    Element authorUserEl = author.getChild("user");
                    if (authorUserEl != null) {
                        authorObj.setUser(authorUserEl.getValue());
                    }

                    Element authorEmailEl = author.getChild("email");
                    if (authorEmailEl != null) {
                        authorObj.setEmail(authorEmailEl.getValue());
                    }
                    authorsTag.add(authorObj);
                }
            }
        }

        //php max min
        el = getNode(packageXpaths.get("phpmin"));
        if (el != null) {
            phpMinimum = el.getValue();
        }

        el = getNode(packageXpaths.get("phpmax"));
        if (el != null) {
            phpMaximum = el.getValue();
        }

    }

    public void createPackage() {

        getContents().removeContent();
        visitChildrenRecursively(myRootFile, new RecursivelyListener() {
            @Override
            public void visited(File file) {
                addFile(file.getPath());
            }
        });
    }

    public void displayXml() {
        XMLOutputter xmOut = new XMLOutputter();
        xmOut.setFormat(Format.getPrettyFormat());
        System.out.println("----" + xmOut.outputString(getDocument()));
    }

    public void addNode(String xpath, Element el) {

    }


    public String getDirType(String path) {
        String dirType = null;
        boolean maybeOther = true;
        for (Iterator it = ALL_PATH.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry mapEntry = (Map.Entry) it.next();
            String key = (String) mapEntry.getKey();
            Object value = mapEntry.getValue();

            if (path.startsWith(key)) {
                maybeOther = false;
                if (key.length() < path.length()) {
                    dirType = (String) value;
                    if (!key.equals(".")) {
                        return dirType;
                    }
                }
            }
        }


        if (dirType == null && maybeOther) {
            dirType = "other";
        }


        return dirType;
    }

    private Map.Entry getDirEntry(String path) {
        Map.Entry mapEntry = null;
        boolean maybeOther = true;
        for (Iterator it = ALL_PATH.entrySet().iterator(); it.hasNext(); ) {
            mapEntry = (Map.Entry) it.next();
            String key = (String) mapEntry.getKey();
            if (path.startsWith(key)) {
                maybeOther = false;
                if (!key.equals(".")) {
                    return mapEntry;
                }
            }
        }

        if (maybeOther) {
            for (Iterator it = ALL_PATH.entrySet().iterator(); it.hasNext(); ) {
                mapEntry = (Map.Entry) it.next();
                if (mapEntry.getValue().toString().equals("other")) {
                    return mapEntry;
                }
            }
        }

        return mapEntry;
    }

    public void addFile(String path) {

        String relativePath;
        if (path.startsWith("/")) {
            relativePath = FileUtil.getRelativePath(myRootFile.getPath(), path, '/');
        } else {
            relativePath = path;
        }
        if (canAddInXml(relativePath)) {
            String dirType = getDirType(relativePath);
            if (dirType != null) {
                Element targetElement = getTargetElement(dirType);

                if (targetElement == null) {
                    targetElement = createTargetElement(dirType);
                }

                addFileInTarget(targetElement, relativePath);
            }
        }

    }

    public void addFileInTarget(Element targetElement, String path) {

        Map.Entry name = getDirEntry(path);
        String key = (String) name.getKey();
        if (!key.equals(".")) {
            path = path.replaceFirst((String) name.getKey(), "");
        } else {
            key = "";
        }
        String[] split = path.split("/");


        String filePath = new File(myRootFile.getPath(), key).getPath();
        String xpath = "/package/contents//target[@name='" + name.getValue() + "']";

        for (String dirName : split) {
            if (dirName.isEmpty())
                continue;

            Element parentEl = getNode(xpath);

            filePath += "/" + dirName;
            File f = new File(filePath);
            if (f.isDirectory()) {
                xpath += "/dir[@name='" + dirName + "']";
            } else {
                xpath += "/file[@name='" + dirName + "']";
            }

            Element node = getNode(xpath);
            if (node == null) {
                if (f.isDirectory()) {
                    node = new Element("dir");
                    node.setAttribute("name", dirName);
                } else {
                    node = new Element("file");
                    node.setAttribute("name", dirName);

                    String md5 = null;
                    try {
                        md5 = com.kodokux.mageidea.util.FileUtil.getFileMd5(f);
                    } catch (FileNotFoundException e) {
                    } catch (FilerException e) {
                    }

                    if (md5 != null)
                        node.setAttribute("hash", md5);
                }
                parentEl.addContent(node);
            }

        }

//
//        System.out.println(filePath);
//        System.out.println(xpath);


    }

    private Element getNode(String xpath) {
        Element target = null;
        try {
            XPath xpathExpression = XPath.newInstance(xpath);
            target = (Element) xpathExpression.selectSingleNode(getDocument());
        } catch (JDOMException e) {
        }
        return target;
    }


    public Element createTargetElement(String dirType) {

        Element targetElement = getTargetElement(dirType);

        if (targetElement == null) {
            targetElement = new Element("target");
            targetElement.setAttribute("name", dirType);
            getContents().addContent(targetElement);
        }

        return targetElement;
    }

    public Element getTargetElement(String dirType) {
        Element target = null;
        try {
            XPath xpathExpression = XPath.newInstance("/package/contents//target[@name='" + dirType + "']");
            target = (Element) xpathExpression.selectSingleNode(getDocument());
        } catch (JDOMException e) {
        }
        return target;
    }


    public Element getContents() {
        Element target = null;
        try {
            XPath xpathExpression = XPath.newInstance("/package/contents[1]");
            target = (Element) xpathExpression.selectSingleNode(getDocument());
            if (target == null) {
                Element rootElement = document.getRootElement();
                target = new Element("contents");
                rootElement.addContent(target);
            }
        } catch (JDOMException e) {
        }
        return target;
    }

    public Document getDocument() {
        return document;
    }

    private boolean canAddInXml(String relativePath) {
        if (relativePath == null)
            return false;
        for (Iterator it = ALL_PATH.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry mapEntry = (Map.Entry) it.next();
            String key = (String) mapEntry.getKey();
            if (key.startsWith(relativePath)) {
                if (!key.equals(relativePath)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void visitChildrenRecursively(@NotNull File dir, RecursivelyListener listener) {
        final File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.getName().equals(".DS_Store") ||
                        file.getName().equals("modman") ||
                        file.getName().equals(".git") ||
                        file.getName().equals("package.xml") ||
                        file.getName().equals("genPackages") ||
                        file.getName().equals(".gitignore")) {
                    continue;
                }
                listener.visited(file);
                if (file.isDirectory()) {
                    visitChildrenRecursively(file, listener);
                }
            }
        }
    }

    public void save() throws IOException {
        save(myPackegeFile);
    }

    public void save(File file) throws IOException {
//        XMLOutputter xmOut = new XMLOutputter();
//        xmOut.setFormat(Format.getPrettyFormat());
//        System.out.println(file.getPath());
//        xmOut.output(getDocument(), new FileWriter(file));
        save(getDocument(), file);
    }

    public void save(Document el, File file) throws IOException {
        XMLOutputter xmOut = new XMLOutputter();
        xmOut.setFormat(Format.getPrettyFormat());
        System.out.println(file.getPath());
        xmOut.output(el, new FileWriter(file));
    }

    private Element setBaseElement(String tagName, String value) {
        Element node = getNode(packageXpaths.get(tagName));
        if (node == null) {
            node = new Element(tagName);
            getDocument().getRootElement().addContent(node);
        }
        node.setText(value);
        return node;
    }

    public String getNameTag() {
        return nameTag;
    }


    public void setNameTag(String nameTag) {
        String tagName = "name";
        Element node = setBaseElement(tagName, nameTag);
        this.nameTag = nameTag;
    }

    public String getVersionTag() {
        return versionTag;
    }

    public void setVersionTag(String versionTag) {
        String tagName = "version";
        Element node = setBaseElement(tagName, versionTag);
        this.versionTag = versionTag;
    }

    public String getStabilityTag() {
        return stabilityTag;
    }

    public void setStabilityTag(String stabilityTag) {
        String tagName = "stability";
        Element node = setBaseElement(tagName, stabilityTag);
        this.stabilityTag = stabilityTag;
    }

    public String getLicenseTag() {
        return licenseTag;
    }

    public void setLicenseTag(String licenseTag, String url) {
        String tagName = "license";
        Element node = setBaseElement(tagName, licenseTag);
        node.setAttribute("uri", url);
        this.licenseTag = licenseTag;
    }

    public String getChannelTag() {
        return channelTag;
    }

    public void setChannelTag(String channelTag) {
        String tagName = "channel";
        Element node = setBaseElement(tagName, channelTag);
        this.channelTag = channelTag;
    }

    public String getSummaryTag() {
        return summaryTag;
    }

    public void setSummaryTag(String summaryTag) {
        String tagName = "summary";
        Element node = setBaseElement(tagName, summaryTag);
        this.summaryTag = summaryTag;
    }

    public String getDescriptionTag() {
        return descriptionTag;
    }

    public void setDescriptionTag(String descriptionTag) {
        String tagName = "description";
        Element node = setBaseElement(tagName, descriptionTag);
        this.descriptionTag = descriptionTag;
    }

    public String getNotesTag() {
        return notesTag;
    }

    public void setNotesTag(String notesTag) {
        String tagName = "notes";
        Element node = setBaseElement(tagName, notesTag);
        this.notesTag = notesTag;
    }

    public String getDateTag() {
        return dateTag;
    }

    public void setDateTag(String dateTag) {
        String tagName = "date";
        Element node = setBaseElement(tagName, dateTag);
        this.dateTag = dateTag;
    }

    public String getTimeTag() {
        return timeTag;
    }

    public void setTimeTag(String timeTag) {
        String tagName = "time";
        Element node = setBaseElement(tagName, timeTag);
        this.timeTag = timeTag;
    }

    public String getExtendsTag() {
        return extendsTag;
    }

    public void setExtendsTag(String extendsTag) {
        this.extendsTag = extendsTag;
    }

    public List<Author> getAuthorsTag() {
        return authorsTag;
    }

    public String getLicenseUrlTag() {
        return licenseUrlTag;
    }

    public String getPhpMaximum() {
        return phpMaximum;
    }

    public String getPhpMinimum() {
        return phpMinimum;
    }

    public void setPhpMaximum(String maximum) {
        String tagName = "phpmax";
        Element node = getNode(packageXpaths.get(tagName));

        if (node == null) {
            String xpath = packageXpaths.get(tagName);
            Element parentEl = findTarAndCreate(xpath);
            if (parentEl.getName().equals("max")) {
                parentEl.setText(maximum);
            }
        }

        this.phpMaximum = dateTag;
    }

    public void setPhpMinimum(String maximum) {
        String tagName = "phpmin";
        Element node = getNode(packageXpaths.get(tagName));
        if (node == null) {
            String xpath = packageXpaths.get(tagName);
            Element parentEl = findTarAndCreate(xpath);
            if (parentEl.getName().equals("min")) {
                parentEl.setText(maximum);
            }
        }
        this.phpMaximum = dateTag;
    }

    private Element findTarAndCreate(String xpath) {
        String[] split = xpath.split("/");
        String searchXpath = "/package";
        Element parentEl = getDocument().getRootElement();
        for (int i = 2; i < split.length; i++) {
            String s = split[i];
            if (s.isEmpty())
                continue;
            searchXpath += "/" + s;
            Element newEl = getNode(searchXpath);
            if (newEl == null) {
                newEl = new Element(s);
                parentEl.addContent(newEl);
            }
            parentEl = newEl;
        }
        return parentEl;
    }

    public void setAuthorsTag(List<Author> authors) {
        String tagName = "authors";
        Element node = getNode(packageXpaths.get(tagName));
        if (node == null) {
            String xpath = packageXpaths.get(tagName);
            Element parentEl = findTarAndCreate(xpath);
            if (parentEl.getName().equals("authors")) {
                node = parentEl;
            }
        }

        if (node != null) {
            node.removeContent();
            for (Author author : authors) {
                Element newAuthor = new Element("author");
                Element authorName = new Element("name");
                authorName.setText(author.getName());

                Element authorEmail = new Element("email");
                authorEmail.setText(author.getEmail());

                Element authorUser = new Element("user");
                authorUser.setText(author.getUser());

                newAuthor.addContent(authorName);
                newAuthor.addContent(authorUser);
                newAuthor.addContent(authorEmail);

                node.addContent(newAuthor);
            }
        }

        this.authorsTag = authors;
    }


    static public class Author {
        private String name;
        private String user;
        private String email;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }


    interface RecursivelyListener {
        void visited(File file);
    }

}
