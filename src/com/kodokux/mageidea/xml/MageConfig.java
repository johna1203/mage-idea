package com.kodokux.mageidea.xml;

import com.intellij.openapi.project.Project;
import com.kodokux.mageidea.service.FileSystem;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/13
 * Time: 17:52
 */
public class MageConfig {
    Document document;
    Element modulesNode;
    Element rootNode;
    Element globalNode;
    Element adminNode;
    Element adminhtmlNode;
    Element installNode;
    Element frontendNode;
    Element defaultNode;

    private File file;
    private String shortName;
    private String extensionName;


    public MageConfig() throws ParserConfigurationException {
        DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docbuilder = dbfactory.newDocumentBuilder();
        this.document = docbuilder.newDocument();
        init();
    }

    private void init() {
        rootNode = document.createElement("config");
        document.appendChild(rootNode);

        //modules
        modulesNode = document.createElement("modules");
        rootNode.appendChild(modulesNode);

        //global
        globalNode = document.createElement("global");
        rootNode.appendChild(globalNode);

        //admin
        adminNode = document.createElement("admin");
        rootNode.appendChild(adminNode);

        //adminhtml
        adminhtmlNode = document.createElement("adminhtml");
        rootNode.appendChild(adminhtmlNode);

        //install
        installNode = document.createElement("install");
        rootNode.appendChild(installNode);

        //frontend
        frontendNode = document.createElement("frontend");
        rootNode.appendChild(frontendNode);

        //default
        defaultNode = document.createElement("default");
        rootNode.appendChild(defaultNode);
    }

    private XPath getXpath() {
        XPathFactory xpfactory = XPathFactory.newInstance();
        return xpfactory.newXPath();
    }

    private Object evaluate(Document document, String path, QName type) {
        XPathExpression expr = null;
        try {
            expr = getXpath().compile(path);
            return expr.evaluate(document, type);
        } catch (XPathExpressionException e) {
        }
        return null;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getShortName() {
        if (shortName == null || shortName.isEmpty()) {
            shortName = getExtensionName().toLowerCase();
        }
        return shortName;
    }

    public void setExtensionName(String shortName) {
        this.extensionName = shortName;
    }

    public String getExtensionName() {
        return extensionName;
    }

    public String getModuleName() {
        String extensionName1 = getExtensionName();
        String[] split = extensionName1.split("_");

        String moduleName = "";
        if (split.length > 0) {
            moduleName = split[1].toLowerCase();
        }
        return moduleName;
    }

    public void setVersion(String moduleName, String version) {
        Node moduleNode = (Node) evaluate(document, "/config/modules/" + moduleName, XPathConstants.NODE);
        if (moduleNode == null) {
            Node modulesNode = (Node) evaluate(document, "/config/modules", XPathConstants.NODE);
            moduleNode = document.createElement(moduleName);
            modulesNode.appendChild(moduleNode);

            Element versionNode = document.createElement("version");
            versionNode.appendChild(document.createTextNode(version));
            moduleNode.appendChild(versionNode);
        } else {
            //TODO : update
            System.out.println("Node type" + moduleNode.getFirstChild().getNodeType());
        }
    }

    protected Element addNode(Element node, String child) {
        Element el = document.createElement(child);
//        String modelBaseClassName = extesionName + "_Model";
        node.appendChild(el);
        return el;
    }

    public void setModels() {
//        <global>
//          <models>
//            <kodokux_payment>
//              <class>Kodokux_Payment_Model</class>
//              <resourceModel>kodokux_payment_resource</resourceModel>
//            </kodokux_payment>
//            <kodokux_payment_resource>
//              <class>Kodokux_Payment_Model_Resource</class>
//              </kodokux_payment_resource>
//          </models>
//        </global>
        Element models = (Element) evaluate(document, "/config/global/models", XPathConstants.NODE);
        if (models == null) {
            models = addNode(globalNode, "models");
        }

        String extensionName = getExtensionName();
        String shortName = getShortName();
        Element shortNameNode = addNode(models, shortName);
        String modelBaseClassName = extensionName + "_Model";

        Element classEl = addNode(shortNameNode, "class");
        classEl.appendChild(document.createTextNode(modelBaseClassName));

        String shortNameResource = shortName + "_resource";
        Element resourceModel = addNode(shortNameNode, "resourceModel");
        resourceModel.appendChild(document.createTextNode(shortNameResource));

        //kodokux_payment_resource
        Element shortNameResourceModel = addNode(models, shortNameResource);
        classEl = addNode(shortNameResourceModel, "class");
        classEl.appendChild(document.createTextNode(modelBaseClassName + "_Resource"));

        initResource();
    }

    public void initResource() {
//        <resources>
//          <kodokux_payment_read>
//            <connection>
//              <use>core_read</use>
//            </connection>
//          </kodokux_payment_read>
//          <kodokux_payment_write>
//            <connection>
//              <use>core_write</use>
//            </connection>
//          </kodokux_payment_write>
//          <kodokux_payment_setup>
//            <connection>
//              <use>core_setup</use>
//            </connection>
//            <setup>
//              <module>Kodokux_Payment</module>
//            </setup>
//          </kodokux_payment_setup>
//        </resources>
        Element resources = (Element) evaluate(document, "/config/global/resources", XPathConstants.NODE);
        if (resources == null) {
            resources = addNode(globalNode, "resources");
        }

        String shortName = getShortName();

        String[] connString = {
                "_read",
                "_write",
                "_setup"
        };

        for (String type : connString) {
            String typeName = shortName + type;
            Element typeNameEl = addNode(resources, typeName);

            Element connection = addNode(typeNameEl, "connection");
            Element use = addNode(connection, "use");
            use.appendChild(document.createTextNode("core" + type));

        }


    }

    public void setHelpers() {
//        <global>
//          <helpers>
//            <(modulename)>
//              <class>(ClassName_Prefix)</class>
//            </(modulename)>
//          </helpers>
//        </global>
        Element helpers = (Element) evaluate(document, "/config/global/helpers", XPathConstants.NODE);
        if (helpers == null) {
            helpers = addNode(globalNode, "helpers");
        }

        String extensionName = getExtensionName();
        String shortName = getShortName();

        Element shortNameNode = addNode(helpers, shortName);
        String helperClassName = extensionName + "_Helper";
        Element classEl = addNode(shortNameNode, "class");
        classEl.appendChild(document.createTextNode(helperClassName));
    }

    public void setBlocks() {
//        <global>
//          <blocks>
//           <(modulename)>
//             <class>(ClassName_Prefix)</class>
//           </(modulename)>
//          </blocks>
//        </global>
        Element helpers = (Element) evaluate(document, "/config/global/blocks", XPathConstants.NODE);
        if (helpers == null) {
            helpers = addNode(globalNode, "blocks");
        }

        String extensionName = getExtensionName();
        String shortName = getShortName();

        Element shortNameNode = addNode(helpers, shortName);
        String helperClassName = extensionName + "_Block";
        Element classEl = addNode(shortNameNode, "class");
        classEl.appendChild(document.createTextNode(helperClassName));
    }

    public void load(File config) throws ParserConfigurationException, IOException, SAXException {
        this.file = config;
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        this.document = dBuilder.parse(config);
    }


    public void save(File config, Project project) {
        this.file = config;
        FileSystem fileSystem = FileSystem.getInstance(project);
        fileSystem.createFile(config, document);


    }

    public void save(Project project) {
        save(file, project);
    }

    public void setAdminRoutes() {
//      <routers>
//        <adminhtml>
//          <args>
//            <modules>
//              <(NameSpace_ModuleName) before="Mage_Adminhtml">
//                (New_ClassName)
//              <(NameSpace_ModuleName)
//          </args>
//        </adminhtml>
//      </routers>
        Element routers = (Element) evaluate(document, "/config/admin/routers", XPathConstants.NODE);
        if (routers == null) {
            routers = addNode(adminNode, "routers");
        }

        Element adminhtml = addNode(routers, "adminhtml");
        Element args = addNode(adminhtml, "args");
        Element modules = addNode(args, "modules");
        Element shortname = addNode(modules, getShortName());
        shortname.setAttribute("before", "Mage_Adminhtml");
        String className = extensionName + "_Adminhtml";
        shortname.appendChild(document.createTextNode(className));

    }

    public void setFrontendRouters() {
//        <routers>
//          <(modulename)>
//            <use>[standard|admin|default]</use>
//            <args>
//              <module>(NameSpace_ModuleName)</module>
//              <frontName>(frontname)</frontName>
//            </args>
//          </(modulename)>
//        </routers>

        Element routers = (Element) evaluate(document, "/config/frontend/routers", XPathConstants.NODE);
        if (routers == null) {
            routers = addNode(frontendNode, "routers");
        }

        Element shortName = addNode(routers, getShortName());
        Element use = addNode(shortName, "use");
        use.appendChild(document.createTextNode("standard"));
        Element args = addNode(shortName, "args");
        Element modules = addNode(args, "modules");
        String className = extensionName;
        modules.appendChild(document.createTextNode(className));
        Element frontName = addNode(args, "frontName");


    }

    public void setSql() {

    }
}
