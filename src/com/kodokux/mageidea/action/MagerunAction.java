package com.kodokux.mageidea.action;

import com.intellij.execution.ExecutionException;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.project.Project;
import com.jetbrains.php.config.PhpCommandLineCommand;
import com.kodokux.mageidea.magerun.actions.SearchExtensionDialog;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/10/01
 * Time: 16:24
 */
public class MagerunAction extends AnAction {


    private String currentPhpCgiPath;
    //    private final AtomicBoolean configurationChangedListenerAdded = new AtomicBoolean();
    private Project project;

    public void actionPerformed(AnActionEvent actionEvent) {

        project = (Project) PlatformDataKeys.PROJECT.getData(actionEvent.getDataContext());
        if (project == null)
            return;


        SearchExtensionDialog dialog = new SearchExtensionDialog(project, project.getBaseDir());

        dialog.show();
        if (dialog.isOK()) {

        }


//        try {
//            PhpCommandLineCommand phpCommandLine = createGeneralCommandLine();
////            phpCommandLine.addArgument("/usr/local/bin/n98-magerun.phar");
////            VirtualFile scriptFile = PhpExecutionUtil.getHelperScriptFile("phpinfo.php");
//            phpCommandLine.setWorkingDirectory(project.getBasePath());
//            phpCommandLine.setScript("/usr/local/bin/n98-magerun.phar");
//            phpCommandLine.addArgument("dev:template-hints");
////
////            String output = ScriptRunnerUtil.getProcessOutput(phpCommandLine.createGeneralCommandLine());
////
////            System.out.println(output);
//            GeneralCommandLine commandLine = phpCommandLine.createGeneralCommandLine();
//            String magerunCommad = MagerunUtils.getMagerunCommad(project);
//            OSProcessHandler processHandler = new OSProcessHandler(commandLine) {
//
//            };
//
////            OSProcessHandler processHandler = ScriptRunnerUtil.execute(magerunCommad, project.getBasePath(), null);
//            processHandler.addProcessListener(new ProcessAdapter() {
//                @Override
//                public void onTextAvailable(ProcessEvent event, Key outputType) {
//                    System.out.println(event.getText());
//
//                }
//
//                @Override
//                public void startNotified(ProcessEvent event) {
//                    System.out.println("johna");
//                }
//
//                @Override
//                public void processWillTerminate(ProcessEvent event, boolean willBeDestroyed) {
//                    System.out.println("processWillTerminate");
//                }
//
//                @Override
//                public void processTerminated(ProcessEvent event) {
//                    System.out.println("processTerminated");
//                }
//            });
//
//            processHandler.startNotify();
//
//
////            commandLine.
//        } catch (ExecutionException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
    }


    public PhpCommandLineCommand createGeneralCommandLine() throws ExecutionException {
        return PhpCommandLineCommand.create(project);
    }


//    private void addConfigurationChangedListener()
//    {
//        if(!configurationChangedListenerAdded.compareAndSet(false, true))
//        {
//            return;
//        } else
//        {
//            project.getMessageBus().connect(this).subscribe(PhpProjectWorkspaceConfiguration.TOPIC, new PhpProjectWorkspaceConfigurationListener() {
//
//                public void interpreterChanged()
//                {
//                    String phpCgiPath = currentPhpCgiPath;
//                    if(phpCgiPath == null)
//                        return;
//                    File file = PhpCgiService.getPhpCgiPath(
//// JavaClassFileOutputException: get_constant: invalid tag
//
//                    final PhpCgiService this$0;
//
//
//                    {
//                        this$0 = PhpCgiService.this;
//                        super();
//                    }
//                }
//                );
//                return;
//            }
//        }

}
