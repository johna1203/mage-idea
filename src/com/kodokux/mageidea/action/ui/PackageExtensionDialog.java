package com.kodokux.mageidea.action.ui;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.ui.AnActionButton;
import com.intellij.ui.AnActionButtonRunnable;
import com.intellij.ui.AnActionButtonUpdater;
import com.intellij.ui.ToolbarDecorator;
import com.intellij.ui.table.JBTable;
import com.kodokux.mageidea.xml.ConfigXml;
import com.kodokux.mageidea.xml.PackageXml;
import com.magicento.ui.dialog.ChooseModuleDialog;
import org.jdom.JDOMException;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/10/11
 * Time: 19:06
 */
public class PackageExtensionDialog extends DialogWrapper {
    private final Project myProject;
    private JPanel mainPanel;
    private JTable authorsTable;
    private JTextField nameTextField;
    private JTextField licenseTextField;
    private JTextField licenseURITextField;
    private JTextArea summaryTextArea;
    private JTextArea descriptionTextArea;
    private JComboBox channelComboBox;
    private JTextField releaseVersionTextField;
    private JTextField releaseStabilityTextField;
    private JTextArea notesTextArea;
    private JTextField minimumTextField;
    private JTextField maximumTextField;
    private JPanel packagesTablePane;
    private JPanel authorTablePane;
    private JPanel extensionsTablePane;
    private JTable packagesTable;

    private AbstractTableModel dependsTableModel;
    private AbstractTableModel authorsTableModel;
    private AbstractTableModel packagesTableModel;
    private List<PackageXml.Author> authorsArray;
    private List<ConfigXml.Depends> dependsArray;

    private PackageXml packageXml = null;
    private File packageXmlFile;

    public List<PackageXml.Author> getAuthors() {
        return authorsArray;
    }

    class MyTabFocusListener extends KeyAdapter {
        private final JTextArea myTextArea;

        MyTabFocusListener(JTextArea textArea) {
            myTextArea = textArea;
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_TAB) {
                if (e.getModifiers() > 0) {
                    myTextArea.transferFocusBackward();
                } else {
                    myTextArea.transferFocus();
                }
                e.consume();
            }
        }
    }

    ;

    public PackageExtensionDialog(Project project) {
        super(project);

        myProject = project;

        init();
    }

    @Override
    protected void init() {

        setOKButtonText("Create Package");

        channelComboBox.addItem("local");
        channelComboBox.addItem("community");
        channelComboBox.addItem("core");


        channelComboBox.setEnabled(false);
        releaseVersionTextField.setEnabled(false);


        summaryTextArea.addKeyListener(new MyTabFocusListener(summaryTextArea));
        descriptionTextArea.addKeyListener(new MyTabFocusListener(descriptionTextArea));
        notesTextArea.addKeyListener(new MyTabFocusListener(notesTextArea));


        ChooseModuleDialog moduleDialog = new ChooseModuleDialog(myProject);
        moduleDialog.show();

        if (moduleDialog.isOK()) {
            String selectedModulePath = moduleDialog.getSelectedModulePath();
            String selectedModule = moduleDialog.getSelectedModule();

            setTitle("Create Extension Package : " + selectedModule);

            packageXmlFile = findPackageXml(selectedModulePath);
            if (packageXmlFile != null) {
                try {
                    packageXml = new PackageXml(packageXmlFile.getParentFile(), packageXmlFile);
                    packageXml.loadXml();
                } catch (JDOMException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            ConfigXml configXml = null;
            File configFile = new File(selectedModulePath, "etc/config.xml");
            if (configFile.exists()) {
                try {
                    configXml = new ConfigXml(configFile);
                    configXml.load();
                } catch (JDOMException e) {
                } catch (IOException e) {
                }
            }


            ConfigXml globalConfigXml = null;
            File ectXml = findEctXml(selectedModulePath, moduleDialog.getSelectedModule());
            if (ectXml.exists()) {
                try {
                    globalConfigXml = new ConfigXml(ectXml);
                    globalConfigXml.load(moduleDialog.getSelectedModule());
                } catch (JDOMException e) {
                } catch (IOException e) {
                }
            }

            dependsArray = new ArrayList<ConfigXml.Depends>();
            if (globalConfigXml != null) {
                channelComboBox.setSelectedItem(globalConfigXml.getCodePoll());
                dependsArray = globalConfigXml.getDepends();

            }


            if (configXml != null) {
                nameTextField.setText(configXml.getModuleName());
                releaseVersionTextField.setText(configXml.getVersion());
            }

            authorsArray = new ArrayList<PackageXml.Author>();
            if (packageXml != null) {

                licenseTextField.setText(packageXml.getLicenseTag());
                licenseURITextField.setText(packageXml.getLicenseUrlTag());
                summaryTextArea.setText(packageXml.getSummaryTag());
                descriptionTextArea.setText(packageXml.getDescriptionTag());
                releaseStabilityTextField.setText(packageXml.getStabilityTag());
                notesTextArea.setText(packageXml.getNotesTag());
                authorsArray = packageXml.getAuthorsTag();
                maximumTextField.setText(packageXml.getPhpMaximum());
                minimumTextField.setText(packageXml.getPhpMinimum());

            }

            initDependsTable(dependsArray);
            initAuthorsTable(authorsArray);
//            initPackageTable(packagesArray);
            authorTablePane.add(createAuthorTable(), BorderLayout.CENTER);
            packagesTablePane.add(createPackageTable(), BorderLayout.CENTER);


        }

        super.init();

    }

    private void initAuthorsTable(final List<PackageXml.Author> authorsArray) {
        authorsTableModel = new AbstractTableModel() {
            String[] columnNames = {"Name", "User", "Email"};


            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return true;
            }

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return authorsArray.size();
            }

            @Override
            public int getColumnCount() {
                return columnNames.length;
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                String value = (String) aValue;
                if (columnIndex == 0) {
                    authorsArray.get(rowIndex).setName(value);
                }

                if (columnIndex == 1) {
                    authorsArray.get(rowIndex).setUser(value);
                }

                if (columnIndex == 2) {
                    authorsArray.get(rowIndex).setEmail(value);
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                String value = null;
                if (columnIndex == 0) {
                    value = authorsArray.get(rowIndex).getName();
                }

                if (columnIndex == 1) {
                    value = authorsArray.get(rowIndex).getUser();
                }

                if (columnIndex == 2) {
                    value = authorsArray.get(rowIndex).getEmail();
                }

                return value;
            }
        };

        authorsTable = new JBTable(authorsTableModel);

    }

    private JComponent createAuthorTable() {
        JPanel panelForTable = ToolbarDecorator.createDecorator(authorsTable)
                .setAddAction(new AnActionButtonRunnable() {
                    @Override
                    public void run(AnActionButton button) {
                        PackageXml.Author author = new PackageXml.Author();
                        author.setName("Change me");
                        author.setEmail("Change me");
                        author.setUser("Change me");
                        authorsArray.add(author);
//                        authorsTableModel.fireTableRowsInserted(authorsArray.size() - 1, authorsArray.size() - 1);
                        authorsTableModel.fireTableStructureChanged();
                        authorsTable.setRowSelectionInterval(authorsArray.size() - 1, authorsArray.size() - 1);
                    }
                }).setEditAction(new AnActionButtonRunnable() {
                    @Override
                    public void run(AnActionButton button) {

                    }
                }).setRemoveAction(new AnActionButtonRunnable() {
                    @Override
                    public void run(AnActionButton button) {
                        if (authorsArray.size() == 1)
                            return;
                        int selectedColumn = authorsTable.getSelectedRow();
                        authorsArray.remove(selectedColumn);
                        authorsTableModel.fireTableStructureChanged();
                        authorsTable.setRowSelectionInterval(authorsArray.size() - 1, authorsArray.size() - 1);
                    }
                }).setAddActionUpdater(new AnActionButtonUpdater() {
                    @Override
                    public boolean isEnabled(AnActionEvent e) {
                        return true;
                    }
                }).setEditActionUpdater(new AnActionButtonUpdater() {
                    @Override
                    public boolean isEnabled(AnActionEvent e) {
                        return false;

                    }
                }).setRemoveActionUpdater(new AnActionButtonUpdater() {
                    @Override
                    public boolean isEnabled(AnActionEvent e) {
                        return true;
                    }
                }).disableUpDownActions().createPanel();
        panelForTable.setPreferredSize(new Dimension(-1, 200));
        return panelForTable;
    }


    private JComponent createPackageTable() {
        JPanel panelForTable = ToolbarDecorator.createDecorator(packagesTable)
                .setAddAction(new AnActionButtonRunnable() {
                    @Override
                    public void run(AnActionButton button) {
                    }
                }).setEditAction(new AnActionButtonRunnable() {
                    @Override
                    public void run(AnActionButton button) {
                    }
                }).setRemoveAction(new AnActionButtonRunnable() {
                    @Override
                    public void run(AnActionButton button) {
                    }
                }).setAddActionUpdater(new AnActionButtonUpdater() {
                    @Override
                    public boolean isEnabled(AnActionEvent e) {
                        return true;
                    }
                }).setEditActionUpdater(new AnActionButtonUpdater() {
                    @Override
                    public boolean isEnabled(AnActionEvent e) {
                        return true;
                    }
                }).setRemoveActionUpdater(new AnActionButtonUpdater() {
                    @Override
                    public boolean isEnabled(AnActionEvent e) {
                        return true;
                    }
                }).disableUpDownActions().createPanel();
        panelForTable.setPreferredSize(new Dimension(-1, 200));
        return panelForTable;
    }


    private void initDependsTable(final List<ConfigXml.Depends> dependsArray) {
        dependsTableModel = new AbstractTableModel() {
            String[] columnNames = {"Package", "Channel", "Min", "Max"};


            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                if (columnIndex == 0) {
                    return false;
                }
                return true;
            }

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return dependsArray.size();
            }

            @Override
            public int getColumnCount() {
                return columnNames.length;
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                String value = (String) aValue;
                if (columnIndex == 0) {
                    dependsArray.get(rowIndex).setPackageName(value);
                }

                if (columnIndex == 1) {
                    dependsArray.get(rowIndex).setChannel(value);
                }

                if (columnIndex == 2) {
                    dependsArray.get(rowIndex).setMin(value);
                }

                if (columnIndex == 3) {
                    dependsArray.get(rowIndex).setMax(value);
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                String value = null;
                if (columnIndex == 0) {
                    value = dependsArray.get(rowIndex).getPackageName();
                }

                if (columnIndex == 1) {
                    value = dependsArray.get(rowIndex).getChannel();
                }

                if (columnIndex == 2) {
                    value = dependsArray.get(rowIndex).getMin();
                }

                if (columnIndex == 3) {
                    value = dependsArray.get(rowIndex).getMax();
                }

                return value;
            }
        };
        packagesTable = new JBTable(dependsTableModel);
        packagesTable.setModel(dependsTableModel);
        TableColumn column = packagesTable.getColumnModel().getColumn(1);
        JComboBox jComboBox = new JComboBox(new Object[]{"", "local", "community", "core"});
        column.setCellEditor(new DefaultCellEditor(jComboBox));
    }

    private File findEctXml(String selectedModulePath, String moduleName) {
        File file = new File(selectedModulePath);

        File appDir = null;
        while (true) {
            if (!file.exists())
                break;
            if (file.getPath().equals(myProject.getBasePath()))
                break;

            if (file.isDirectory() && file.getName().equals("app")) {
                appDir = file;
                break;
            }
            file = file.getParentFile();
        }

        if (appDir != null) {
            File moduleDir = new File(appDir, "etc/modules");
            if (moduleDir.exists()) {
                File etcXml = new File(moduleDir, moduleName + ".xml");
                if (etcXml.exists()) {
                    return etcXml;
                } else {
                    File[] files = moduleDir.listFiles();
                    if (files != null && files.length > 0) {

                    }
                }
            }
        }
        return file;
    }


    private File findPackageXml(String selectedModulePath) {
        File file = new File(selectedModulePath);
        File rootPath = null;
        if (file.exists()) {
            while (true) {
                if (!file.exists())
                    break;
                if (file.getPath().equals(myProject.getBasePath()))
                    break;
                File[] list = file.listFiles();
                if (list != null) {
                    for (File children : list) {
                        File parent = children.getParentFile().getParentFile();
                        if (parent != null && parent.getName().equals(".modman")) {
                            rootPath = children.getParentFile();
                            if (children.getName().equals("package.xml")) {
                                return children;
                            }
                        }

                    }
                }
                file = file.getParentFile();
            }
        }

        if (rootPath != null) {
            return new File(rootPath, "package.xml");

        }

        return null;
    }


    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        mainPanel.setPreferredSize(new Dimension(500, 600));
        return mainPanel;
    }

    public String getName() {
        return nameTextField.getText();
    }

    public String getLicense() {
        return licenseTextField.getText();
    }

    public String getLicenseURI() {
        return licenseURITextField.getText();
    }

    public String getSummary() {
        return summaryTextArea.getText();
    }

    public String getDescription() {
        return descriptionTextArea.getText();
    }

    public String getChannel() {
        return (String) channelComboBox.getSelectedItem();
    }

    public String getReleaseVersion() {
        return releaseVersionTextField.getText();
    }

    public String getReleaseStability() {
        return releaseStabilityTextField.getText();
    }

    public String getNotes() {
        return notesTextArea.getText();
    }

    public String getMinimum() {
        return minimumTextField.getText();
    }

    public String getMaximum() {
        return maximumTextField.getText();
    }

    public PackageXml getPackageXml() {
        return packageXml;
    }

    public File getPackageXmlFile() {
        return packageXmlFile;
    }

}
