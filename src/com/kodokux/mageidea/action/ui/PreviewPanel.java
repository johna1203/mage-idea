package com.kodokux.mageidea.action.ui;

import com.intellij.openapi.project.Project;
import com.intellij.psi.codeStyle.CodeStyleSettingsManager;
import com.jetbrains.php.lang.formatter.ui.PhpCodeStylePanel;

/**
 * User: johna1203
 * Date: 2013/09/22
 * Time: 2:04
 */
public class PreviewPanel extends PhpCodeStylePanel {

    protected PreviewPanel(Project project) {
        super(CodeStyleSettingsManager.getInstance(project).getCurrentSettings());
    }
}
