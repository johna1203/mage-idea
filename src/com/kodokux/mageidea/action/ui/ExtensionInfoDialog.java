package com.kodokux.mageidea.action.ui;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.List;

public class ExtensionInfoDialog extends DialogWrapper {


    private JComboBox extensionListComboBox;
    private JPanel contentPanel;

    public ExtensionInfoDialog(@Nullable Project project, List<String> mageConfig) {
        super(project);


//        jPanel = new JPanel();
//        jPanel.add(version);
//        jPanel.add(name);

        for (String name : mageConfig) {
            extensionListComboBox.addItem(name);
        }

        init();
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return contentPanel;
    }
}
