package com.kodokux.mageidea.action.ui;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.EditorFactory;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.util.Condition;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.util.PsiTreeUtil;
import com.jetbrains.php.lang.psi.PhpFile;
import com.jetbrains.php.lang.psi.PhpPsiUtil;
import com.jetbrains.php.lang.psi.elements.Method;
import com.jetbrains.php.lang.psi.elements.PhpClass;
import com.jetbrains.php.refactoring.PhpNameUtil;
import com.jetbrains.php.run.PhpRunUtil;
import com.kodokux.mageidea.module.Extension;
import com.kodokux.mageidea.service.Magento;
import com.kodokux.mageidea.templates.MagentoFileFromTemplateDataProvider;
import com.kodokux.mageidea.util.PhpElementsUtil;
import com.kodokux.mageidea.xml.ConfigXmlUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import java.awt.*;
import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/17
 * Time: 18:52
 */
public class OverrideDialog extends DialogWrapper implements MagentoFileFromTemplateDataProvider {
    private final Project myProject;
    private final PsiFile myFile;
    private final Editor myEditor;
    private Extension overrideExtension;
    private ConfigXmlUtil.Rewrite[] rewriteExtension;
    private String rewriteDirType = null;
    private String rewriteConfigType = null;
    private PhpClass aClass = null;
    private PsiDirectory myDirectory;
    private final Magento magento;
    private JPanel mainContent;
    private JLabel textLabel;
    private JComboBox extensionNameComboBox;
    private JComboBox extendsClassComboBox;
    private JCheckBox otherExtensionOverrideThisCheckBox;
    private JPanel editorPanel;
    private List<Extension> extensionList;
    private String classContext = "";

    public OverrideDialog(Project project, PsiDirectory directory, Editor editor, PsiFile file) {
        super(project);
        myProject = project;
        myDirectory = directory;
        myFile = file;
        myEditor = editor;

        magento = Magento.getInstance(project);

        if (myFile instanceof PhpFile) {
            Collection<PhpClass> aClasses = PhpPsiUtil.findClasses((PhpFile) myFile, Condition.TRUE);
            if (aClasses.size() > 0) {
                aClass = aClasses.isEmpty() ? null : aClasses.iterator().next();
            }
        }

        VirtualFile virtualFile = myFile.getVirtualFile();
        if (virtualFile != null) {
            rewriteDirType = magento.getFileType(new File(virtualFile.getPath()));
            if (rewriteDirType.equals("Block")) {
                rewriteConfigType = "blocks";
            } else if (rewriteDirType.equals("Model")) {
                rewriteConfigType = "models";
            } else if (rewriteDirType.equals("Helper")) {
                rewriteConfigType = "helpers";
            }
        }

        if (rewriteConfigType == null) {
            return;
        }


        setTitle("Override Dialog");
        textLabel.setText("johna");

        rewriteExtension = magento.getRewriteExtension(myFile.getVirtualFile());
        if (rewriteExtension.length > 0) {
            otherExtensionOverrideThisCheckBox.setVisible(false);
        } else {
            otherExtensionOverrideThisCheckBox.setVisible(true);
        }

        File file1 = null;
        if (virtualFile == null) {
            return;
        }
        file1 = new File(virtualFile.getPath());
        String channel = magento.getChannel(file1);
        overrideExtension = new Extension(magento.getExtensionName(file1), channel, project);


        init();
    }

//    protected ValidationInfo doValidate() {
//        if (otherExtensionOverrideThisCheckBox.isVisible() && !otherExtensionOverrideThisCheckBox.isSelected()) {
//            return new ValidationInfo("Has rewrite", otherExtensionOverrideThisCheckBox);
//        }
//        return null;
//    }
//
//    //
//    protected boolean postponeValidation() {
//        return true;
//    }

    @Override
    protected void init() {

        List<Extension> extensionList = magento.getExtensionList();
        extensionNameComboBox.setModel(new DefaultComboBoxModel(extensionList.toArray()));
        if (myFile instanceof PhpFile) {
            Collection<PhpClass> aClasses = PhpPsiUtil.findClasses((PhpFile) myFile, Condition.TRUE);
            PhpClass aClass = null;
            if (aClasses.size() > 0) {
                aClass = aClasses.isEmpty() ? null : aClasses.iterator().next();
            }
            if (aClass != null) {

                PhpClass[] classExtendsClasses = PhpElementsUtil.getClassExtendsClasses(aClass);
                DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel(classExtendsClasses);

                BasicComboBoxRenderer boxRenderer = new BasicComboBoxRenderer() {
                    @Override
                    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                        if (value != null) {
                            PhpClass item = (PhpClass) value;
                            setText(item.getName());
                        }
                        return this;
                    }
                };

                extendsClassComboBox.setRenderer(boxRenderer);
                extendsClassComboBox.setModel(comboBoxModel);

//                int indexOfLast = ArrayUtil.indexOf(classExtendsClasses, aClass);
//                if (indexOfLast > -1)
//                    extendsClassComboBox.setSelectedIndex(indexOfLast);
//                else
                extendsClassComboBox.setSelectedIndex(0);
            }


            int offset = myEditor.getCaretModel().getOffset();
            PsiElement elementAt = myFile.findElementAt(offset);
            Method tests = PsiTreeUtil.getParentOfType(elementAt, Method.class);
            if (tests != null) {
                classContext = tests.getText();
//                EditorFactory editorFactory = EditorFactory.getInstance();
//                Document document = editorFactory.createDocument(classContext);
//                Editor viewer = editorFactory.createViewer(document, myProject);
//                editorPanel.add(viewer.getComponent());
            }


        } else {
            System.out.println("IS aClass is null");
        }


        super.init();    //To change body of overridden methods use File | Settings | File Templates.
    }

    protected void doOKAction() {
        Extension selectedItem = (Extension) extensionNameComboBox.getSelectedItem();
//        VirtualFileSystem fileSystem = VirtualFileManager.getInstance().getFileSystem(selectedItem.getRootPath().getPath());
//        fileSystem.
        File rootPath = selectedItem.getRootPath();

        if (rootPath != null) {
            VirtualFile fileByIoFile = LocalFileSystem.getInstance().findFileByIoFile(rootPath);
            if (fileByIoFile != null) {
                myDirectory = PsiManager.getInstance(myProject).findDirectory(fileByIoFile);
            }
        }

        super.doOKAction();
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return mainContent;
    }

    public void setExtensionList(List<Extension> extensionList) {
        this.extensionList = extensionList;
        for (Extension extension : extensionList) {
            extensionNameComboBox.addItem(extension);
        }
    }

    public List<Extension> getExtensionList() {
        return extensionList;
    }

    public JComponent getPreferredFocusedComponent() {
        return extensionNameComboBox;
    }

    @NotNull
    @Override
    public String getTemplateName() {
        return "magento_class";
    }

    @NotNull
    @Override
    public String getFilePath() {
        PsiDirectory directory = getDirectory();

        String name = aClass.getName();
//        int inx;
//        if ((inx = name.indexOf("_")) != -1) {
//            name = name.substring(inx);
//        }

        String extensionName = overrideExtension.getExtensionName();

        System.out.println("this :" + extensionName + "_" + rewriteDirType);
        System.out.println("to this :" + overrideExtension.getModuleName());

        name = name.replace(extensionName + "_" + rewriteDirType, overrideExtension.getModuleName());

        System.out.println("File name " + name);

        String path = name.replaceAll("_", File.separator);
        File file = new File("/" + rewriteDirType + File.separator + path + File.separator).getParentFile();
        File file1 = new File(file, getFileName());
        return PhpNameUtil.getFullFileName(file1.getPath(), getExtension());
    }

    @NotNull
    @Override
    public PsiDirectory getBaseDirectory() {
        return getDirectory();
    }

    @NotNull
    @Override
    public Properties getProperties(@NotNull PsiDirectory psiDirectory) {
        PhpClass selectedItem = (PhpClass) extendsClassComboBox.getSelectedItem();
        Properties properties = new Properties();
        String basePath = getDirectory().getVirtualFile().getPath();
        String moduleName = overrideExtension.getModuleName();
        String s = moduleName + "_" + rewriteDirType;
        String filePath = getFilePath();


        String sPath = s.replace("_", File.separator);
        String sPath2 = moduleName.replace("_", File.separator);
        String s1 = filePath.replaceFirst(sPath, sPath2);
        File file = new File(basePath, s1);
        properties.setProperty("CLASSNAME", magento.getClassName(file));
        properties.setProperty("EXTENDSCLASSNAME", selectedItem.getName());
        properties.setProperty("METHODNAME", classContext);
        return properties;
    }

    @Override
    public VirtualFile getConfigXml() {
        Extension selectedItem = (Extension) extensionNameComboBox.getSelectedItem();
        String configPath = selectedItem.getRootPath().getPath()
                + File.separator + "etc" + File.separator + "config.xml";
        return PhpRunUtil.findFile(configPath);
    }

    @Override
    public ConfigXmlUtil.Rewrite getRewrite() {

        final Properties properties = getProperties(getBaseDirectory());
        VirtualFile virtualFile = myFile.getVirtualFile();
        String extensionName = overrideExtension.getExtensionName();
        String aClassName = aClass.getName();
        final String replace = aClassName.replaceFirst(extensionName + "_" + rewriteDirType + "_", "");
//
//        System.out.println("extensionName  : " + extensionName + "_" + rewriteDirType);
//        System.out.println("replace  : " + replace);

        if (virtualFile != null) {
            return new ConfigXmlUtil.Rewrite() {
                @Override
                public String getRewriteType() {
                    return rewriteConfigType;
                }

                @Override
                public String getTagName() {
                    return overrideExtension.getExtensionModelTagName();
                }

                @Override
                public String getRewriteClassShortName() {
                    return replace.toLowerCase();
                }

                @Override
                public String getRewriteClass() {
                    return properties.getProperty("CLASSNAME");
                }
            };
        }
        return null;
    }

    private String getExtension() {
        return "php";
    }

    private String getFileName() {
        String name = myFile.getName();
        if (name.lastIndexOf(".php") == name.length() - 4) {
            name = name.substring(0, name.length() - 4);
        }
        return name;
    }

    protected PsiDirectory getDirectory() {
        Extension selectedItem = (Extension) extensionNameComboBox.getSelectedItem();
        VirtualFile directory = PhpRunUtil.findDirectory(selectedItem.getRootPath().getPath());
        if (directory != null) {
            PsiDirectory psiDirectory = PsiManager.getInstance(myProject).findDirectory(directory);
            if (psiDirectory != null)
                return psiDirectory;
        }
        return null;
    }

}
