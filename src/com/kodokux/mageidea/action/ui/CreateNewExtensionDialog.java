package com.kodokux.mageidea.action.ui;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;

public class CreateNewExtensionDialog extends DialogWrapper {
    private JPanel contentPane;
    private JTextField vendorNameTextField;
    private JTextField moduleNameTextField;
    private JTextField versionTextField;
    private JComboBox channelComboBox;

    public CreateNewExtensionDialog(Project project) {
        super(project, true);
        setTitle("Create Extension");
        contentPane.setPreferredSize(new Dimension(500, 500));
        init();
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return contentPane;
    }


    public String getVendorName() {
        char first = Character.toUpperCase(vendorNameTextField.getText().charAt(0));
        return first + vendorNameTextField.getText().substring(1);
    }

    public String getModuleName() {
        char first = Character.toUpperCase(moduleNameTextField.getText().charAt(0));
        return first + moduleNameTextField.getText().substring(1);
    }

    public String getChannel() {
        return (String) channelComboBox.getSelectedItem();
    }

    public String getVersion() {
        return (String) versionTextField.getText();
    }
}
