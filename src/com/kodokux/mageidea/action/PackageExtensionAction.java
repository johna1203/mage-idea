package com.kodokux.mageidea.action;

import com.intellij.notification.NotificationType;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.kodokux.mageidea.MagentoComponent;
import com.kodokux.mageidea.action.ui.PackageExtensionDialog;
import com.kodokux.mageidea.util.TgzCompressor;
import com.kodokux.mageidea.util.ZipCompressor;
import com.kodokux.mageidea.xml.PackageXml;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/10/11
 * Time: 19:03
 */
public class PackageExtensionAction extends AnAction {
    @Override
    public void actionPerformed(AnActionEvent e) {

        DataContext dataContext = e.getDataContext();
        Project project = (Project) PlatformDataKeys.PROJECT.getData(dataContext);
        if (project == null)
            return;

        PackageExtensionDialog dialog = new PackageExtensionDialog(project);

        dialog.show();
        if (dialog.isOK()) {
            String name = dialog.getName();
            String channel = dialog.getChannel();
            String description = dialog.getDescription();
            String license = dialog.getLicense();
            String licenseURI = dialog.getLicenseURI();
            String maximum = dialog.getMaximum();
            String minimum = dialog.getMinimum();
            String notes = dialog.getNotes();
            String releaseStability = dialog.getReleaseStability();
            String releaseVersion = dialog.getReleaseVersion();
            String summary = dialog.getSummary();
            List<PackageXml.Author> authors = dialog.getAuthors();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            Date date1 = new Date();

            PackageXml packageXml = dialog.getPackageXml();
            packageXml.setChannelTag(channel);
            packageXml.setNameTag(name);
            packageXml.setDescriptionTag(description);
            packageXml.setLicenseTag(license, licenseURI);
            packageXml.setNotesTag(notes);
            packageXml.setStabilityTag(releaseStability);
            packageXml.setVersionTag(releaseVersion);
            packageXml.setSummaryTag(summary);
            packageXml.setPhpMinimum(minimum);
            packageXml.setPhpMaximum(maximum);
            packageXml.setAuthorsTag(authors);
            packageXml.setDateTag(dateFormat.format(date1));
            packageXml.setTimeTag(timeFormat.format(date1));
            packageXml.createPackage();
            try {
                packageXml.save(dialog.getPackageXmlFile());
//                ZipCompressor zipCompressor = new ZipCompressor(dialog.getPackageXmlFile().getParentFile());
//                zipCompressor.archive();
                TgzCompressor compressor = new TgzCompressor();
                compressor.setSrcFile(dialog.getPackageXmlFile().getParentFile());

                File packagesDir = new File(dialog.getPackageXmlFile().getParentFile(), "genPackages");
                if (!packagesDir.exists()) {
                    packagesDir.mkdir();
                }

                String fileName = name + releaseVersion + ".tgz";
                File destFile = new File(packagesDir, fileName);
                compressor.setSrcFile(dialog.getPackageXmlFile().getParentFile());
                compressor.setDstFile(destFile);
                compressor.save();

                LocalFileSystem.getInstance().refreshAndFindFileByIoFile(dialog.getPackageXmlFile());


                MagentoComponent.showInLogEvent("Success", "package.xml save");
            } catch (IOException e1) {
                MagentoComponent.showInLogEvent("Error on Save", "package.xml" + e1.getMessage(), NotificationType.ERROR);
            }
        }


    }
}
