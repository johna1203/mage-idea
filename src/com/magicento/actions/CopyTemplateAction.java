package com.magicento.actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.*;
import com.magicento.MagicentoSettings;
import com.magicento.helpers.IdeHelper;
import com.magicento.models.layout.Template;
import com.magicento.ui.dialog.ChooseModuleDialog;
import com.magicento.ui.dialog.CopyTemplateDialog;

import java.io.File;
import java.io.IOException;

/**
 * @author Enrique Piatti
 */
public class CopyTemplateAction extends MagicentoActionAbstract {
    public void executeAction() {

        final Project project = getProject();

        final VirtualFile file = getVirtualFile();
        Template template = new Template(file);

        ChooseModuleDialog moduleDialog = new ChooseModuleDialog(project);
        moduleDialog.show();

        if (moduleDialog.isOK()) {
            String selectedModule = moduleDialog.getSelectedModule();
            String selectedModulePath = moduleDialog.getSelectedModulePath();

            File moduleRoot = findRoot(selectedModulePath);

            final CopyTemplateDialog dialog = new CopyTemplateDialog(project, template);
            dialog.show();
            if (dialog.isOK()) {

                String packageName = dialog.getPackage();
                String theme = dialog.getTheme();
                MagicentoSettings settings = MagicentoSettings.getInstance(project);
                if (settings != null) {
                    String pathToMagento = moduleRoot.getPath();
                    if (pathToMagento != null && !pathToMagento.isEmpty()) {
                        String newFilePath = moduleRoot.getPath() + "/app/design/" + template.getArea() + "/" + packageName + "/" + theme + "/template/" + template.getRelativePath();
                        final File newFile = new File(newFilePath);
                        if (newFile.exists()) {
                            if (!IdeHelper.prompt("File " + newFilePath + " already exists\nDo you want to override it?", "File already exists")) {
                                return;
                            }
                        }

                        ApplicationManager.getApplication().runWriteAction(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    final VirtualFile newParent = VfsUtil.createDirectories(newFile.getParent());
                                    if (newParent != null) {
                                        VirtualFile copy = VfsUtilCore.copyFile(this, file, newParent);
                                        openFile(copy);
                                    } else {
                                        IdeHelper.logError("Cannot create parent directories for: " + newFile.getAbsolutePath());
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    public File findRoot(String selectedModulePath) {

        File file = new File(selectedModulePath);
        String projectRoot = _project.getBasePath();
        File projectRootFile = new File(projectRoot);

        File moduleRootFile = null;


        while (true) {
            File parent = file.getParentFile();

            if (!file.exists())
                break;

            if (projectRootFile.getPath().equals(file.getPath()) || file.getName().equals(".modman"))
                break;

            if (parent.getName().equals(".modman")) {
                moduleRootFile = file;
                break;
            }


            file = file.getParentFile();
        }


        return moduleRootFile;
    }


    @Override
    public Boolean isApplicable(AnActionEvent e) {
        setEvent(e);
        VirtualFile file = getVirtualFile();
        Template template = new Template(file);
        return template.isTemplate();
    }
}
