package com.magicento.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.magicento.MagicentoProjectComponent;
import com.magicento.helpers.XmlHelper;
import org.jdom.Element;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * @author Enrique Piatti
 */
public class NewModuleDialog extends DialogWrapper {

    protected Project project;
    private JPanel mainPanel;
    private JComboBox codePoolComboBox;
    private JTextField nameSpaceTextField;
    private JTextField moduleNameTextField;
    private JTextField groupNameTextField;
    private JTextField versionTextField;
    private JCheckBox modelCheckBox;
    private JCheckBox helperCheckBox;
    private JCheckBox blockCheckBox;
    private JCheckBox installerCheckBox;
    private JTextField gitURLTextField;
    private JList dependsList;

    public NewModuleDialog(Project project) {
        super(project);
        this.project = project;
        mainPanel.setPreferredSize(new Dimension(500, 800));
        init();
        setTitle("New Module");
    }

    @Override
    protected void init() {
//        codePool.addItem("local");
//        codePool.addItem("community");

        fillDepends();

        versionTextField.setText("0.9.0");

        modelCheckBox.setSelected(true);
        helperCheckBox.setSelected(true);
        blockCheckBox.setSelected(true);
        installerCheckBox.setSelected(true);

        super.init();
    }

    private void fillDepends() {
        dependsList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        MagicentoProjectComponent magicento = MagicentoProjectComponent.getInstance(project);
        File configXml = magicento.getCachedConfigXml();
        List<Element> modules = XmlHelper.findXpath(configXml, "config/modules/*");
        if (modules != null && modules.size() > 0) {
            String[] moduleNames = new String[modules.size()];
            int i = 0;
            for (Element module : modules) {
                moduleNames[i] = module.getName();
                i++;
            }
            Arrays.sort(moduleNames);
            dependsList.setListData(moduleNames);
        }
    }

    @Override
    protected JComponent createCenterPanel() {
        return mainPanel;
    }

    public String getCodePool() {
        return (String) codePoolComboBox.getSelectedItem();
    }

    public String getNamespace() {
        return nameSpaceTextField.getText();
    }

    public String getModule() {
        return moduleNameTextField.getText();
    }

    public String getVersion() {
        return versionTextField.getText();
    }

    public String getGroup() {
        return groupNameTextField.getText();
    }

    public String[] getDepends() {
        return Arrays.copyOf(dependsList.getSelectedValues(), dependsList.getSelectedValues().length, String[].class);
        // Arrays.asList(Object_Array).toArray(new String[Object_Array.length]);
        // return (String[])depends.getSelectedValues();
    }

    public boolean includeHelper() {
        return helperCheckBox.isSelected();
    }

    public boolean includeBlock() {
        return blockCheckBox.isSelected();
    }

    public boolean includeModel() {
        return modelCheckBox.isSelected();
    }

    public boolean includeInstaller() {
        return installerCheckBox.isSelected();
    }

    public String getGitUrl() {
        return gitURLTextField.getText();
    }

    @Override
    public JComponent getPreferredFocusedComponent() {
        return nameSpaceTextField;
    }
}
