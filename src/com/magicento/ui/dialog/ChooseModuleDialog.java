package com.magicento.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.magicento.MagicentoSettings;
import com.magicento.helpers.FileHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Enrique Piatti
 */
public class ChooseModuleDialog extends DialogWrapper {

    protected final String LOCAL = "local";
    protected final String COMMUNITY = "community";

    protected Project project;

    protected Map<String, String> communityModules;
    protected Map<String, String> localModules;

    protected String selectedModulePath;
    protected String selectedPool;
    private JComboBox codePoolComboBox;
    private JComboBox moduleNameComboBox;
    private JPanel mainPanel;

    public ChooseModuleDialog(@org.jetbrains.annotations.Nullable Project project) {
        super(project);
        this.project = project;
        communityModules = new LinkedHashMap<String, String>();
        localModules = new LinkedHashMap<String, String>();
        init();
        setTitle("Choose target Module");
    }

    @Override
    protected JComponent createCenterPanel() {

        codePoolComboBox.setSelectedItem(LOCAL);

        updateModulesComboItems(localModules);
        String moduleName = (String) moduleNameComboBox.getSelectedItem();
        selectedPool = LOCAL;
        selectedModulePath = localModules.get(moduleName);

        codePoolComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String codePool = (String) ((JComboBox) e.getSource()).getSelectedItem();
                if (codePool.equals(LOCAL)) {
                    updateModulesComboItems(localModules);
                } else {
                    updateModulesComboItems(communityModules);
                }
                pack();
            }
        });

        moduleNameComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String moduleName = (String) ((JComboBox) e.getSource()).getSelectedItem();
                String selectedPool = (String) codePoolComboBox.getSelectedItem();
                if (selectedPool.equals(LOCAL)) {
                    selectedModulePath = localModules.get(moduleName);
                } else {
                    selectedModulePath = communityModules.get(moduleName);
                }
            }
        });

        mainPanel.setPreferredSize(new Dimension(-1, -1));
        return mainPanel;
    }

    @Override
    protected void init() {
        codePoolComboBox.addItem(LOCAL);
        codePoolComboBox.addItem(COMMUNITY);

        fillModules();

        super.init();
    }

    private void fillModules() {
        MagicentoSettings settings = MagicentoSettings.getInstance(project);
        if (settings != null) {
            String basePath = settings.getPathToMagento() + "/app/code/";
            fillModulePaths(basePath + LOCAL, localModules);
            fillModulePaths(basePath + COMMUNITY, communityModules);
        }

    }

    protected void fillModulePaths(String base, Map<String, String> map) {
        String packageName;
        String moduleName;
        File etcConfig;
        File pool = new File(base);
        for (File packageFile : FileHelper.getSubdirectoriesFiles(pool)) {
            packageName = packageFile.getName();
            for (File moduleFile : FileHelper.getSubdirectoriesFiles(packageFile)) {

                try {
                    File canonicalFile = moduleFile.getCanonicalFile();
                    etcConfig = new File(canonicalFile.getAbsolutePath() + "/etc/config.xml");
                    if (etcConfig.exists()) {
                        moduleName = moduleFile.getName();
                        map.put(packageName + "_" + moduleName, canonicalFile.getAbsolutePath());
                    }
                } catch (IOException e) {
                }


            }
        }
    }

    protected void updateModulesComboItems(Map<String, String> map) {
        moduleNameComboBox.removeAllItems();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            moduleNameComboBox.addItem(entry.getKey());
        }
    }


    public String getSelectedModulePath() {
        return selectedModulePath.replace("\\", "/");
    }

    public String getSelectedPool() {
        return selectedPool;
    }

    public String getSelectedModule() {
        return (String) moduleNameComboBox.getSelectedItem();
    }

}
