package com.magicento.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.psi.PsiElement;
import com.magicento.MagicentoProjectComponent;
import com.magicento.helpers.PsiPhpHelper;
import com.magicento.helpers.XmlHelper;
import org.jdom.Element;

import javax.swing.*;
import java.io.File;
import java.util.List;

/**
 * @author Enrique Piatti
 */
public class RewriteControllerDialog extends DialogWrapper {
    private final Project myProject;

//    protected JTextField subfolder;
//    protected JComboBox before;
//    protected JComboBox parentClassName;

    protected PsiElement currentElement;
    protected String area;
    protected Element routerElement;
    private JPanel mainPanel;
    private JTextField subfolderTextField;
    private JComboBox beforeComboBox;
    private JComboBox classNameComboBox;
    private String selectedModulePath;
    private ChooseModuleDialog chooseModuleDialog;

    public RewriteControllerDialog(@org.jetbrains.annotations.Nullable Project project, PsiElement currentElement) {
        super(project);
        myProject = project;
        this.currentElement = currentElement;
        init();
        setTitle("Rewrite Controller");
    }


    @Override
    protected JComponent createCenterPanel() {
//        JPanel panel = (JPanel) super.createCenterPanel();
//        panel.add(new JLabel("Subfolder"));
//        panel.add(subfolder);
//        panel.add(new JLabel("Before"));
//        panel.add(before);
//        panel.add(new JLabel("Extends"));
//        panel.add(parentClassName);
//        panel.add(mainPanel);
        return mainPanel;
    }

    @Override
    protected void init() {
        if (currentElement != null) {
            initRouterElement();
            initArea();
            initSubfolder();
            initBefore();
            initParentClassName();
            super.init();
        }
    }

    protected void initParentClassName() {
        String className = PsiPhpHelper.getClassName(currentElement);
        String originalParent = PsiPhpHelper.getParentClassName(currentElement);
        String defaultClass = getArea().equals("frontend") ? "Mage_Core_Controller_Front_Action" : "Mage_Adminhtml_Controller_Action";
        String[] options = originalParent.equals(defaultClass) ? new String[]{defaultClass, className} : new String[]{defaultClass, originalParent, className};
        for (String option : options) {
            classNameComboBox.addItem(option);
        }
    }

    protected void initBefore() {
        String module = getRouterModule();
        String[] beforeOptions = module != null ? new String[]{module, "-"} : new String[]{"-"};
        for (String option : beforeOptions) {
            beforeComboBox.addItem(option);
        }
//        before = new JComboBox(beforeOptions);
    }

    protected void initSubfolder() {
//        String subf = "";
//        String parentClassName = PsiPhpHelper.getParentClassName(currentElement);
//        if(parentClassName != null && parentClassName.equals("Mage_Adminhtml_Controller_Action")){
//            subf = "Adminhtml";
//        }
        String subf = getArea().equals("admin") ? "Adminhtml" : "";
//        subfolder = new JTextField(subf);
        subfolderTextField.setText(subf);
        subfolderTextField.setToolTipText("subfolder inside /controllers/ this is normally used for Adminhtml controllers");
    }

    @Override
    public void show() {
        chooseModuleDialog = new ChooseModuleDialog(myProject);
        chooseModuleDialog.show();
        if (chooseModuleDialog.isOK()) {
            super.show();
        }
    }

    protected void initRouterElement() {
        Element bestMatch = null;
        String className = PsiPhpHelper.getClassName(currentElement);
        File configXml = MagicentoProjectComponent.getInstance(myProject).getCachedConfigXml();
        String xpath = "//routers/*/args/module|//routers/*/args/modules/*";
        List<Element> routers = XmlHelper.findXpath(configXml, xpath);
        if (routers != null) {
            for (Element router : routers) {
                String value = router.getValue().trim();
                if (className.contains(value) && (bestMatch == null || bestMatch.getValue().trim().length() < value.length())) {
                    bestMatch = router;
                }
            }
        }
        routerElement = bestMatch;
    }

    protected void initArea() {
        if (routerElement != null) {
            if (routerElement.getName().equals("module")) {
                area = routerElement.getParentElement().getParentElement().getParentElement().getParentElement().getName();
            } else {
                area = routerElement.getParentElement().getParentElement().getParentElement().getParentElement().getParentElement().getName();
            }
        } else {
            area = "frontend";
        }
    }

    public String getArea() {
        return area;
    }


    public String getRouterName() {
        if (routerElement != null) {
            if (routerElement.getName().equals("module")) {
                return routerElement.getParentElement().getParentElement().getName();
            } else {
                return routerElement.getParentElement().getParentElement().getParentElement().getName();
            }
        }
        return null;
    }

    public String getRouterModule() {
        if (routerElement != null) {
            return routerElement.getValue().trim();
        }
        return null;
    }

    public String getBefore() {
        return (String) beforeComboBox.getSelectedItem();
    }

    public String getSubfolder() {
        return subfolderTextField.getText().trim();
    }

    public String getParentClassName() {
        return (String) classNameComboBox.getSelectedItem();
    }

    public String getSelectedModule() {
        return chooseModuleDialog.getSelectedModule();
    }

    public String getSelectedModulePath() {
        return chooseModuleDialog.getSelectedModulePath();
    }
}
