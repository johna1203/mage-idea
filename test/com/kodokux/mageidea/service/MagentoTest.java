package com.kodokux.mageidea.service;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.testFramework.UsefulTestCase;
import com.yourkit.util.Asserts;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class MagentoTest extends UsefulTestCase {
    @Test
    public void testClassname() {
        File helper = new File("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/core/Johna/Test/Helper/Data.php");
        //project
        Project project = Mockito.mock(Project.class);
        when(project.getBasePath()).thenReturn("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013");

//        assertThat(file.getPath(), is("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/core/Johna/Test/Helper"));

        Magento magento = new Magento(project);
        String className = magento.getClassName(helper);

        assertThat(className, equalTo("Johna_Test_Helper_Data"));


        File model = new File("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/core/Johna/Test/Model/Product.php");
        className = magento.getClassName(model);

        assertThat(className, equalTo("Johna_Test_Model_Product"));

        model = new File("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/core/Johna/Test/Model/Resource/Product.php");
        className = magento.getClassName(model);

        assertThat(className, equalTo("Johna_Test_Model_Resource_Product"));

    }

    @Test
    public void testModulename() {
        File helper = new File("/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/core/ECGKodokux/Test/Helper/Data.php");

        //project
        Project project = Mockito.mock(Project.class);
        when(project.getBasePath()).thenReturn("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013");

//        assertThat(file.getPath(), is("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/core/Johna/Test/Helper"));

        Magento magento = new Magento(project);
        String className = magento.getExtensionName(helper);

        assertThat(className, equalTo("ECGKodokux_Test"));
    }

    @Test
    public void testGetControllerClassName() {
        File helper = new File("/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/core/ECGKodokux/Test/controllers/Catalog/indexController.php");

        //project
        Project project = Mockito.mock(Project.class);
        when(project.getBasePath()).thenReturn("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013");

//        assertThat(file.getPath(), is("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/core/Johna/Test/Helper"));

        Magento magento = new Magento(project);
        String className = magento.getClassName(helper);

//        System.out.println(className);
        assertThat(className, equalTo("ECGKodokux_Test_Catalog_IndexController"));

    }

    @Test
    public void testGetChannel() {
        File helper = new File("/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/community/ECGKodokux/Test/controllers/Catalog/indexController.php");

        //project
        Project project = Mockito.mock(Project.class);
        when(project.getBasePath()).thenReturn("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013");

//        assertThat(file.getPath(), is("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/core/Johna/Test/Helper"));

        Magento magento = new Magento(project);
        String className = magento.getChannel(helper);

        System.out.println(className);
        assertThat(className, equalTo("community"));
    }

    @Test
    public void testGetRootPath() throws Exception {
        File helper = new File("/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/community/ECGKodokux/Test/controllers/Catalog/indexController.php");
        //project
        Project project = Mockito.mock(Project.class);
        when(project.getBasePath()).thenReturn("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013");

        VirtualFile virtualFile = Mockito.mock(VirtualFile.class);
        when(virtualFile.getPath()).thenReturn("/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/community/ECGKodokux/Test/controllers/Catalog");


        Magento magento = new Magento(project);
        File className = magento.getRootPath(virtualFile);

        Asserts.assertEqual(className.getPath(), equalTo("/IdeaProjectsPlugin/bugathon_march_2013/.modman/johna_test/app/code/community/ECGKodokux/Test"));

    }

}
