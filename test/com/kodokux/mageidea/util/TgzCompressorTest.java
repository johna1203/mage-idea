package com.kodokux.mageidea.util;

import org.junit.Test;

import java.io.File;

/**
 * User: johna1203
 * Date: 2013/10/13
 * Time: 5:12
 */
public class TgzCompressorTest {
    @Test
    public void testSave() throws Exception {
        TgzCompressor compressor = new TgzCompressor();
        compressor.setDstFile(new File("/tmp/johna.tar.gz"));
        compressor.setSrcFile(new File("/Users/johna1203/IdeaProjectsPlugin/bugathon_march_2013/.modman/ecgkodokux_qrcode"));
        compressor.save();
    }
}
