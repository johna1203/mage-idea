package com.kodokux.mageidea.xml;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import com.intellij.testFramework.PlatformTestCase;
import org.apache.commons.lang.ArrayUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

/**
 * Created with IntelliJ IDEA by me!
 * User: johna
 * Date: 2013/09/23
 * Time: 20:54
 */
public final class ConfigXmlUtilTest extends PlatformTestCase {

    private Project project;

//    public ConfigXmlUtilTest() {
//        PlatformTestCase.initPlatformLangPrefix();
//    }

    @Before
    public void setUp() throws Exception {
        super.setUp();


        project = getProject();

//        final TestFixtureBuilder<IdeaProjectTestFixture> projectBuilder = IdeaTestFixtureFactory.getFixtureFactory().createLightFixtureBuilder();
//        myFixture = IdeaTestFixtureFactory.getFixtureFactory().createCodeInsightFixture(projectBuilder.getFixture());
//        myFixture.setUp();
//        try {
//            myProjectFixture = IdeaTestFixtureFactory.getFixtureFactory().createFixtureBuilder(getTestName(true)).getFixture();
//            myProjectFixture.setUp();
//        } catch (Exception e) {
//            super.tearDown();
//            throw e;
//        }

    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void testGetExtensionName() throws Exception {
        VirtualFile fileByIoFile = LocalFileSystem.getInstance().findFileByIoFile(new File("/Users/johna/IdeaProjects/Mage-idea/test/com/kodokux/mageidea/resource/test2.xml"));
        XmlFile xmlFile = (XmlFile) PsiManager.getInstance(project).findFile(fileByIoFile);

        if (xmlFile != null) {
//            XmlTag rootTag = xmlFile.getRootTag();
//            System.out.println(rootTag.getName());
//            DomManager domManager = DomManager.getDomManager(xmlFile.getProject());

//            Document domDocument = DomPsiConverter.convert(xmlFile);

            String path = "config/global/models/checkout";
            XmlTag root = xmlFile.getRootTag();
            String[] pathElements = path.split("/");
            pathElements = (String[]) ArrayUtils.remove(pathElements, 0); // ArrayUtils.removeElement(pathElements, pathElements[0]);

            XmlTag lastExistentParent = root;
            if (lastExistentParent != null) {
                for (String curTagName : pathElements) {
                    if (lastExistentParent != null) {
                        XmlTag tag = lastExistentParent.findFirstSubTag(curTagName);
                        if (tag == null) {
                            XmlTag newTag = lastExistentParent.createChildTag(curTagName, "", "", false);
                            lastExistentParent = (XmlTag) lastExistentParent.addAfter(newTag, null);
                        } else {
                            lastExistentParent = tag;
                        }
                    }
                }
            }
        }
    }

//    @Test
//    public void testGetExtensionVersion() throws Exception {
//
//    }
//
//    @Test
//    public void testGetExtensionModelShortName() throws Exception {
//
//    }
//
//    @Test
//    public void testGetExtensionHelperShortName() throws Exception {
//
//    }
//
//    @Test
//    public void testGetExtensionResourceModel() throws Exception {
//
//    }
//
//    @Test
//    public void testGetEvents() throws Exception {
//
//    }
//
//    @Test
//    public void testGetRewrites() throws Exception {
//
//    }
//
//    @Test
//    public void testGetOverloadController() throws Exception {
//        VirtualFile virtualFile = Mockito.mock(VirtualFile.class);
//        when(virtualFile.getUrl()).thenReturn("file:///Users/johna/PhpstormPluginProjects/bugathon_march_2013/.modman/ecgkodokux_voucher/app/code/community/ECGKodokux/Voucher/etc/config.xml");
//
//
//        ConfigXmlUtil.OverloadController[] testOverloadControllers = new ConfigXmlUtil.OverloadController[]{
//                new ConfigXmlUtil.OverloadController() {
//                    @Override
//                    public String getType() {
//                        return "routers";
//                    }
//
//                    @Override
//                    public String getRootName() {
//                        return "adminhtml";
//                    }
//
//                    @Override
//                    public String getNameSpace() {
//                        return "kvoucher";
//                    }
//
//                    @Override
//                    public String getBefore() {
//                        return "Mage_Adminhtml";
//                    }
//
//                    @Override
//                    public String getClassName() {
//                        return "ECGKodokux_Voucher_Adminhtml";
//                    }
//                },
//                new ConfigXmlUtil.OverloadController() {
//                    @Override
//                    public String getType() {
//                        return "routers";
//                    }
//
//                    @Override
//                    public String getRootName() {
//                        return "checkout";
//                    }
//
//                    @Override
//                    public String getNameSpace() {
//                        return "ECGKodokux_Voucher";
//                    }
//
//                    @Override
//                    public String getBefore() {
//                        return "Mage_Checkout";
//                    }
//
//                    @Override
//                    public String getClassName() {
//                        return "ECGKodokux_Voucher_Checkout";
//                    }
//                }
//        };
//
//
////        ConfigXmlUtil.OverloadController[] overloadControllers = ConfigXmlUtil.getOverloadController(virtualFile);
//
////        for (ConfigXmlUtil.OverloadController controller : overloadControllers) {
////            System.out.println("getType : " + controller.getType());
////            System.out.println("getRootName : " + controller.getRootName());
////            System.out.println("getNameSpace : " + controller.getNameSpace());
////            System.out.println("getBefore : " + controller.getBefore());
////            System.out.println("getClassName : " + controller.getClassName());
////
////            System.out.println("------------------------------------------------------");
////        }
////
////        for (ConfigXmlUtil.OverloadController controller : testOverloadControllers) {
////            System.out.println("getType : " + controller.getType());
////            System.out.println("getRootName : " + controller.getRootName());
////            System.out.println("getNameSpace : " + controller.getNameSpace());
////            System.out.println("getBefore : " + controller.getBefore());$SELECTION$$END$
////            System.out.println("getClassName : " + controller.getClassName());
////
////            System.out.println("------------------------------------------------------");
////        }
//
////        System.out.println(ArrayUtils.toString(testOverloadControllers));
////        System.out.println(ArrayUtils.toString(overloadControllers));
//
//
////        Assert.assertArrayEquals(testOverloadControllers, overloadControllers);
//    }
//
//    @Test
//    public void testGetElementIfNotExistCreate() throws Exception {
//        VirtualFile virtualFile = Mockito.mock(VirtualFile.class);
//        when(virtualFile.getUrl()).thenReturn("file:///Users/johna/PhpstormPluginProjects/bugathon_march_2013/.modman/ecgkodokux_qrcode/app/code/community/ECGKodokux/QRCode/etc/config.xml");
//
//        Element el = new Element("type_onepage").setText("ECGKodokux_Voucher_Model_Type_Onepage!!!!!!!!!!");
//        el.addContent(new Element("arg").setText("arg"));
//
//        Element elementIfNotExistCreate = ConfigXmlUtil.getElementIfNotExistCreate(virtualFile, "/config/global/models/checkout/rewrite", el);
//
////        XMLOutputter xmlOutput = new XMLOutputter();
////
////        // display nice nice
////        xmlOutput.setFormat(Format.getPrettyFormat());
////        xmlOutput.output(elementIfNotExistCreate, System.out);
//
//
//    }

}
